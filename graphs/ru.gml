Creator "igraph version 1.2.2 Tue Sep 11 15:42:03 2018"
Version 1
graph
[
  directed 0
  layout 6.68740304976422
  node
  [
    id 0
    community 1
    color 1
  ]
  node
  [
    id 1
    community 1
    color 1
  ]
  node
  [
    id 2
    community 1
    color 1
  ]
  node
  [
    id 3
    community 1
    color 1
  ]
  node
  [
    id 4
    community 1
    color 1
  ]
  node
  [
    id 5
    community 1
    color 1
  ]
  node
  [
    id 6
    community 1
    color 1
  ]
  node
  [
    id 7
    community 1
    color 1
  ]
  node
  [
    id 8
    community 1
    color 1
  ]
  node
  [
    id 9
    community 1
    color 1
  ]
  node
  [
    id 10
    community 1
    color 1
  ]
  node
  [
    id 11
    community 1
    color 1
  ]
  node
  [
    id 12
    community 1
    color 1
  ]
  node
  [
    id 13
    community 1
    color 1
  ]
  node
  [
    id 14
    community 1
    color 1
  ]
  node
  [
    id 15
    community 1
    color 1
  ]
  node
  [
    id 16
    community 1
    color 1
  ]
  node
  [
    id 17
    community 1
    color 1
  ]
  node
  [
    id 18
    community 1
    color 1
  ]
  node
  [
    id 19
    community 1
    color 1
  ]
  node
  [
    id 20
    community 2
    color 2
  ]
  node
  [
    id 21
    community 2
    color 2
  ]
  node
  [
    id 22
    community 2
    color 2
  ]
  node
  [
    id 23
    community 2
    color 2
  ]
  node
  [
    id 24
    community 2
    color 2
  ]
  node
  [
    id 25
    community 2
    color 2
  ]
  node
  [
    id 26
    community 2
    color 2
  ]
  node
  [
    id 27
    community 2
    color 2
  ]
  node
  [
    id 28
    community 2
    color 2
  ]
  node
  [
    id 29
    community 2
    color 2
  ]
  node
  [
    id 30
    community 2
    color 2
  ]
  node
  [
    id 31
    community 2
    color 2
  ]
  node
  [
    id 32
    community 2
    color 2
  ]
  node
  [
    id 33
    community 2
    color 2
  ]
  node
  [
    id 34
    community 2
    color 2
  ]
  node
  [
    id 35
    community 2
    color 2
  ]
  node
  [
    id 36
    community 2
    color 2
  ]
  node
  [
    id 37
    community 2
    color 2
  ]
  node
  [
    id 38
    community 2
    color 2
  ]
  node
  [
    id 39
    community 2
    color 2
  ]
  node
  [
    id 40
    community 3
    color 3
  ]
  node
  [
    id 41
    community 3
    color 3
  ]
  node
  [
    id 42
    community 3
    color 3
  ]
  node
  [
    id 43
    community 3
    color 3
  ]
  node
  [
    id 44
    community 3
    color 3
  ]
  node
  [
    id 45
    community 3
    color 3
  ]
  node
  [
    id 46
    community 3
    color 3
  ]
  node
  [
    id 47
    community 3
    color 3
  ]
  node
  [
    id 48
    community 3
    color 3
  ]
  node
  [
    id 49
    community 3
    color 3
  ]
  node
  [
    id 50
    community 3
    color 3
  ]
  node
  [
    id 51
    community 3
    color 3
  ]
  node
  [
    id 52
    community 3
    color 3
  ]
  node
  [
    id 53
    community 3
    color 3
  ]
  node
  [
    id 54
    community 3
    color 3
  ]
  node
  [
    id 55
    community 3
    color 3
  ]
  node
  [
    id 56
    community 3
    color 3
  ]
  node
  [
    id 57
    community 3
    color 3
  ]
  node
  [
    id 58
    community 3
    color 3
  ]
  node
  [
    id 59
    community 3
    color 3
  ]
  edge
  [
    source 5
    target 3
  ]
  edge
  [
    source 7
    target 1
  ]
  edge
  [
    source 7
    target 3
  ]
  edge
  [
    source 7
    target 5
  ]
  edge
  [
    source 8
    target 0
  ]
  edge
  [
    source 8
    target 1
  ]
  edge
  [
    source 8
    target 6
  ]
  edge
  [
    source 10
    target 3
  ]
  edge
  [
    source 10
    target 4
  ]
  edge
  [
    source 11
    target 2
  ]
  edge
  [
    source 11
    target 7
  ]
  edge
  [
    source 11
    target 9
  ]
  edge
  [
    source 12
    target 1
  ]
  edge
  [
    source 12
    target 2
  ]
  edge
  [
    source 12
    target 10
  ]
  edge
  [
    source 13
    target 6
  ]
  edge
  [
    source 13
    target 7
  ]
  edge
  [
    source 13
    target 10
  ]
  edge
  [
    source 14
    target 1
  ]
  edge
  [
    source 14
    target 4
  ]
  edge
  [
    source 14
    target 8
  ]
  edge
  [
    source 15
    target 1
  ]
  edge
  [
    source 15
    target 12
  ]
  edge
  [
    source 16
    target 6
  ]
  edge
  [
    source 16
    target 11
  ]
  edge
  [
    source 16
    target 13
  ]
  edge
  [
    source 17
    target 0
  ]
  edge
  [
    source 17
    target 1
  ]
  edge
  [
    source 17
    target 5
  ]
  edge
  [
    source 17
    target 6
  ]
  edge
  [
    source 17
    target 12
  ]
  edge
  [
    source 18
    target 8
  ]
  edge
  [
    source 18
    target 13
  ]
  edge
  [
    source 19
    target 12
  ]
  edge
  [
    source 19
    target 15
  ]
  edge
  [
    source 23
    target 22
  ]
  edge
  [
    source 24
    target 20
  ]
  edge
  [
    source 24
    target 22
  ]
  edge
  [
    source 25
    target 22
  ]
  edge
  [
    source 25
    target 24
  ]
  edge
  [
    source 26
    target 22
  ]
  edge
  [
    source 26
    target 23
  ]
  edge
  [
    source 26
    target 24
  ]
  edge
  [
    source 29
    target 11
  ]
  edge
  [
    source 29
    target 19
  ]
  edge
  [
    source 29
    target 22
  ]
  edge
  [
    source 29
    target 25
  ]
  edge
  [
    source 29
    target 27
  ]
  edge
  [
    source 30
    target 25
  ]
  edge
  [
    source 31
    target 17
  ]
  edge
  [
    source 31
    target 22
  ]
  edge
  [
    source 31
    target 27
  ]
  edge
  [
    source 31
    target 28
  ]
  edge
  [
    source 32
    target 22
  ]
  edge
  [
    source 33
    target 21
  ]
  edge
  [
    source 33
    target 22
  ]
  edge
  [
    source 33
    target 23
  ]
  edge
  [
    source 33
    target 27
  ]
  edge
  [
    source 33
    target 32
  ]
  edge
  [
    source 34
    target 21
  ]
  edge
  [
    source 35
    target 21
  ]
  edge
  [
    source 35
    target 22
  ]
  edge
  [
    source 35
    target 24
  ]
  edge
  [
    source 35
    target 28
  ]
  edge
  [
    source 35
    target 30
  ]
  edge
  [
    source 36
    target 21
  ]
  edge
  [
    source 37
    target 9
  ]
  edge
  [
    source 37
    target 28
  ]
  edge
  [
    source 38
    target 24
  ]
  edge
  [
    source 38
    target 26
  ]
  edge
  [
    source 38
    target 28
  ]
  edge
  [
    source 38
    target 29
  ]
  edge
  [
    source 38
    target 35
  ]
  edge
  [
    source 39
    target 8
  ]
  edge
  [
    source 39
    target 30
  ]
  edge
  [
    source 39
    target 37
  ]
  edge
  [
    source 39
    target 38
  ]
  edge
  [
    source 40
    target 15
  ]
  edge
  [
    source 43
    target 41
  ]
  edge
  [
    source 44
    target 2
  ]
  edge
  [
    source 44
    target 40
  ]
  edge
  [
    source 45
    target 13
  ]
  edge
  [
    source 45
    target 18
  ]
  edge
  [
    source 45
    target 20
  ]
  edge
  [
    source 45
    target 41
  ]
  edge
  [
    source 45
    target 43
  ]
  edge
  [
    source 46
    target 14
  ]
  edge
  [
    source 46
    target 41
  ]
  edge
  [
    source 46
    target 43
  ]
  edge
  [
    source 47
    target 8
  ]
  edge
  [
    source 47
    target 45
  ]
  edge
  [
    source 48
    target 43
  ]
  edge
  [
    source 48
    target 44
  ]
  edge
  [
    source 49
    target 9
  ]
  edge
  [
    source 49
    target 41
  ]
  edge
  [
    source 50
    target 23
  ]
  edge
  [
    source 50
    target 42
  ]
  edge
  [
    source 50
    target 45
  ]
  edge
  [
    source 50
    target 46
  ]
  edge
  [
    source 50
    target 47
  ]
  edge
  [
    source 51
    target 42
  ]
  edge
  [
    source 51
    target 48
  ]
  edge
  [
    source 52
    target 36
  ]
  edge
  [
    source 52
    target 40
  ]
  edge
  [
    source 52
    target 43
  ]
  edge
  [
    source 52
    target 48
  ]
  edge
  [
    source 53
    target 36
  ]
  edge
  [
    source 53
    target 45
  ]
  edge
  [
    source 53
    target 46
  ]
  edge
  [
    source 54
    target 1
  ]
  edge
  [
    source 54
    target 43
  ]
  edge
  [
    source 54
    target 53
  ]
  edge
  [
    source 55
    target 43
  ]
  edge
  [
    source 55
    target 52
  ]
  edge
  [
    source 55
    target 53
  ]
  edge
  [
    source 56
    target 47
  ]
  edge
  [
    source 56
    target 53
  ]
  edge
  [
    source 57
    target 45
  ]
  edge
  [
    source 57
    target 48
  ]
  edge
  [
    source 57
    target 49
  ]
  edge
  [
    source 57
    target 55
  ]
  edge
  [
    source 57
    target 56
  ]
  edge
  [
    source 58
    target 41
  ]
  edge
  [
    source 58
    target 43
  ]
  edge
  [
    source 58
    target 49
  ]
  edge
  [
    source 58
    target 52
  ]
  edge
  [
    source 58
    target 54
  ]
  edge
  [
    source 58
    target 57
  ]
  edge
  [
    source 59
    target 39
  ]
  edge
  [
    source 59
    target 42
  ]
  edge
  [
    source 59
    target 55
  ]
]
