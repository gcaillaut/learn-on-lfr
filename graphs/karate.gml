Creator "igraph version 1.2.0 Tue Jul 24 13:45:35 2018"
Version 1
graph
[
  directed 0
  node
  [
    id 1
    community 1
  ]
  node
  [
    id 2
    community 1
  ]
  node
  [
    id 3
    community 1
  ]
  node
  [
    id 4
    community 1
  ]
  node
  [
    id 5
    community 1
  ]
  node
  [
    id 6
    community 1
  ]
  node
  [
    id 7
    community 1
  ]
  node
  [
    id 8
    community 1
  ]
  node
  [
    id 9
    community 2
  ]
  node
  [
    id 10
    community 2
  ]
  node
  [
    id 11
    community 1
  ]
  node
  [
    id 12
    community 1
  ]
  node
  [
    id 13
    community 1
  ]
  node
  [
    id 14
    community 1
  ]
  node
  [
    id 15
    community 2
  ]
  node
  [
    id 16
    community 2
  ]
  node
  [
    id 17
    community 1
  ]
  node
  [
    id 18
    community 1
  ]
  node
  [
    id 19
    community 2
  ]
  node
  [
    id 20
    community 1
  ]
  node
  [
    id 21
    community 2
  ]
  node
  [
    id 22
    community 1
  ]
  node
  [
    id 23
    community 2
  ]
  node
  [
    id 24
    community 2
  ]
  node
  [
    id 25
    community 2
  ]
  node
  [
    id 26
    community 2
  ]
  node
  [
    id 27
    community 2
  ]
  node
  [
    id 28
    community 2
  ]
  node
  [
    id 29
    community 2
  ]
  node
  [
    id 30
    community 2
  ]
  node
  [
    id 31
    community 2
  ]
  node
  [
    id 32
    community 2
  ]
  node
  [
    id 33
    community 2
  ]
  node
  [
    id 34
    community 2
  ]
  edge
  [
    source 2
    target 1
  ]
  edge
  [
    source 3
    target 1
  ]
  edge
  [
    source 3
    target 2
  ]
  edge
  [
    source 4
    target 1
  ]
  edge
  [
    source 4
    target 2
  ]
  edge
  [
    source 4
    target 3
  ]
  edge
  [
    source 5
    target 1
  ]
  edge
  [
    source 6
    target 1
  ]
  edge
  [
    source 7
    target 1
  ]
  edge
  [
    source 7
    target 5
  ]
  edge
  [
    source 7
    target 6
  ]
  edge
  [
    source 8
    target 1
  ]
  edge
  [
    source 8
    target 2
  ]
  edge
  [
    source 8
    target 3
  ]
  edge
  [
    source 8
    target 4
  ]
  edge
  [
    source 9
    target 1
  ]
  edge
  [
    source 9
    target 3
  ]
  edge
  [
    source 10
    target 3
  ]
  edge
  [
    source 11
    target 1
  ]
  edge
  [
    source 11
    target 5
  ]
  edge
  [
    source 11
    target 6
  ]
  edge
  [
    source 12
    target 1
  ]
  edge
  [
    source 13
    target 1
  ]
  edge
  [
    source 13
    target 4
  ]
  edge
  [
    source 14
    target 1
  ]
  edge
  [
    source 14
    target 2
  ]
  edge
  [
    source 14
    target 3
  ]
  edge
  [
    source 14
    target 4
  ]
  edge
  [
    source 17
    target 6
  ]
  edge
  [
    source 17
    target 7
  ]
  edge
  [
    source 18
    target 1
  ]
  edge
  [
    source 18
    target 2
  ]
  edge
  [
    source 20
    target 1
  ]
  edge
  [
    source 20
    target 2
  ]
  edge
  [
    source 22
    target 1
  ]
  edge
  [
    source 22
    target 2
  ]
  edge
  [
    source 26
    target 24
  ]
  edge
  [
    source 26
    target 25
  ]
  edge
  [
    source 28
    target 3
  ]
  edge
  [
    source 28
    target 24
  ]
  edge
  [
    source 28
    target 25
  ]
  edge
  [
    source 29
    target 3
  ]
  edge
  [
    source 30
    target 24
  ]
  edge
  [
    source 30
    target 27
  ]
  edge
  [
    source 31
    target 2
  ]
  edge
  [
    source 31
    target 9
  ]
  edge
  [
    source 32
    target 1
  ]
  edge
  [
    source 32
    target 25
  ]
  edge
  [
    source 32
    target 26
  ]
  edge
  [
    source 32
    target 29
  ]
  edge
  [
    source 33
    target 3
  ]
  edge
  [
    source 33
    target 9
  ]
  edge
  [
    source 33
    target 15
  ]
  edge
  [
    source 33
    target 16
  ]
  edge
  [
    source 33
    target 19
  ]
  edge
  [
    source 33
    target 21
  ]
  edge
  [
    source 33
    target 23
  ]
  edge
  [
    source 33
    target 24
  ]
  edge
  [
    source 33
    target 30
  ]
  edge
  [
    source 33
    target 31
  ]
  edge
  [
    source 33
    target 32
  ]
  edge
  [
    source 34
    target 9
  ]
  edge
  [
    source 34
    target 10
  ]
  edge
  [
    source 34
    target 14
  ]
  edge
  [
    source 34
    target 15
  ]
  edge
  [
    source 34
    target 16
  ]
  edge
  [
    source 34
    target 19
  ]
  edge
  [
    source 34
    target 20
  ]
  edge
  [
    source 34
    target 21
  ]
  edge
  [
    source 34
    target 23
  ]
  edge
  [
    source 34
    target 24
  ]
  edge
  [
    source 34
    target 27
  ]
  edge
  [
    source 34
    target 28
  ]
  edge
  [
    source 34
    target 29
  ]
  edge
  [
    source 34
    target 30
  ]
  edge
  [
    source 34
    target 31
  ]
  edge
  [
    source 34
    target 32
  ]
  edge
  [
    source 34
    target 33
  ]
]
