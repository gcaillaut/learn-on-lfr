Creator "igraph version 1.2.0 Wed May 23 12:03:37 2018"
Version 1
graph
[
  directed 0
  node
  [
    id 0
    name "Wake Forest Demon Deacons"
    community "Atlantic Coast"
  ]
  node
  [
    id 1
    name "Boston College Eagles"
    community "Atlantic Coast"
  ]
  node
  [
    id 2
    name "Maryland Terrapins"
    community "Atlantic Coast"
  ]
  node
  [
    id 3
    name "Clemson Tigers"
    community "Atlantic Coast"
  ]
  node
  [
    id 4
    name "Florida State Seminoles"
    community "Atlantic Coast"
  ]
  node
  [
    id 5
    name "NC State Wolfpack"
    community "Atlantic Coast"
  ]
  node
  [
    id 6
    name "Georgia Tech Yellow Jackets"
    community "Atlantic Coast"
  ]
  node
  [
    id 7
    name "Virginia Tech Hokies"
    community "Atlantic Coast"
  ]
  node
  [
    id 8
    name "Virginia Cavaliers"
    community "Atlantic Coast"
  ]
  node
  [
    id 9
    name "Miami Hurricanes"
    community "Atlantic Coast"
  ]
  node
  [
    id 10
    name "North Carolina Tar Heels"
    community "Atlantic Coast"
  ]
  node
  [
    id 11
    name "Duke Blue Devils"
    community "Atlantic Coast"
  ]
  node
  [
    id 12
    name "Nebraska Cornhuskers"
    community "Big 12"
  ]
  node
  [
    id 13
    name "Missouri Tigers"
    community "Big 12"
  ]
  node
  [
    id 14
    name "Kansas State Wildcats"
    community "Big 12"
  ]
  node
  [
    id 15
    name "Kansas Jayhawks"
    community "Big 12"
  ]
  node
  [
    id 16
    name "Colorado Buffaloes"
    community "Big 12"
  ]
  node
  [
    id 17
    name "Iowa State Cyclones"
    community "Big 12"
  ]
  node
  [
    id 18
    name "Oklahoma Sooners"
    community "Big 12"
  ]
  node
  [
    id 19
    name "Texas Longhorns"
    community "Big 12"
  ]
  node
  [
    id 20
    name "Texas A&M Aggies"
    community "Big 12"
  ]
  node
  [
    id 21
    name "Texas Tech Red Raiders"
    community "Big 12"
  ]
  node
  [
    id 22
    name "Oklahoma State Cowboys"
    community "Big 12"
  ]
  node
  [
    id 23
    name "Baylor Bears"
    community "Big 12"
  ]
  node
  [
    id 24
    name "Louisville Cardinals"
    community "Big East"
  ]
  node
  [
    id 25
    name "Rutgers Scarlet Knights"
    community "Big East"
  ]
  node
  [
    id 26
    name "West Virginia Mountaineers"
    community "Big East"
  ]
  node
  [
    id 27
    name "South Florida Bulls"
    community "Big East"
  ]
  node
  [
    id 28
    name "Cincinnati Bearcats"
    community "Big East"
  ]
  node
  [
    id 29
    name "Pittsburgh Panthers"
    community "Big East"
  ]
  node
  [
    id 30
    name "Connecticut Huskies"
    community "Big East"
  ]
  node
  [
    id 31
    name "Syracuse Orange"
    community "Big East"
  ]
  node
  [
    id 32
    name "Ohio State Buckeyes"
    community "Big Ten"
  ]
  node
  [
    id 33
    name "Wisconsin Badgers"
    community "Big Ten"
  ]
  node
  [
    id 34
    name "Michigan Wolverines"
    community "Big Ten"
  ]
  node
  [
    id 35
    name "Penn State Nittany Lions"
    community "Big Ten"
  ]
  node
  [
    id 36
    name "Purdue Boilermakers"
    community "Big Ten"
  ]
  node
  [
    id 37
    name "Minnesota Golden Gophers"
    community "Big Ten"
  ]
  node
  [
    id 38
    name "Indiana Hoosiers"
    community "Big Ten"
  ]
  node
  [
    id 39
    name "Iowa Hawkeyes"
    community "Big Ten"
  ]
  node
  [
    id 40
    name "Northwestern Wildcats"
    community "Big Ten"
  ]
  node
  [
    id 41
    name "Michigan State Spartans"
    community "Big Ten"
  ]
  node
  [
    id 42
    name "Illinois Fighting Illini"
    community "Big Ten"
  ]
  node
  [
    id 43
    name "Southern Mississippi Golden Eagles"
    community "community USA"
  ]
  node
  [
    id 44
    name "East Carolina Pirates"
    community "community USA"
  ]
  node
  [
    id 45
    name "Marshall Thundering Herd"
    community "community USA"
  ]
  node
  [
    id 46
    name "UCF Knights"
    community "community USA"
  ]
  node
  [
    id 47
    name "UAB Blazers"
    community "community USA"
  ]
  node
  [
    id 48
    name "Memphis Tigers"
    community "community USA"
  ]
  node
  [
    id 49
    name "Houston Cougars"
    community "community USA"
  ]
  node
  [
    id 50
    name "Rice Owls"
    community "community USA"
  ]
  node
  [
    id 51
    name "Tulsa Golden Hurricane"
    community "community USA"
  ]
  node
  [
    id 52
    name "SMU Mustangs"
    community "community USA"
  ]
  node
  [
    id 53
    name "UTEP Miners"
    community "community USA"
  ]
  node
  [
    id 54
    name "Tulane Green Wave"
    community "community USA"
  ]
  node
  [
    id 55
    name "Navy Midshipmen"
    community "FBS Independants"
  ]
  node
  [
    id 56
    name "Army Black Knights"
    community "FBS Independants"
  ]
  node
  [
    id 57
    name "Notre Dame Fighting Irish"
    community "FBS Independants"
  ]
  node
  [
    id 58
    name "Temple Owls"
    community "FBS Independants"
  ]
  node
  [
    id 59
    name "Ohio Bobcats"
    community "Mid-American"
  ]
  node
  [
    id 60
    name "Kent State Golden Flashes"
    community "Mid-American"
  ]
  node
  [
    id 61
    name "Akron Zips"
    community "Mid-American"
  ]
  node
  [
    id 62
    name "Bowling Green Falcons"
    community "Mid-American"
  ]
  node
  [
    id 63
    name "Miami (OH) RedHawks"
    community "Mid-American"
  ]
  node
  [
    id 64
    name "Buffalo Bulls"
    community "Mid-American"
  ]
  node
  [
    id 65
    name "Central Michigan Chippewas"
    community "Mid-American"
  ]
  node
  [
    id 66
    name "Western Michigan Broncos"
    community "Mid-American"
  ]
  node
  [
    id 67
    name "Northern Illinois Huskies"
    community "Mid-American"
  ]
  node
  [
    id 68
    name "Ball State Cardinals"
    community "Mid-American"
  ]
  node
  [
    id 69
    name "Toledo Rockets"
    community "Mid-American"
  ]
  node
  [
    id 70
    name "Eastern Michigan Eagles"
    community "Mid-American"
  ]
  node
  [
    id 71
    name "BYU Cougars"
    community "Mountain West"
  ]
  node
  [
    id 72
    name "TCU Horned Frogs"
    community "Mountain West"
  ]
  node
  [
    id 73
    name "Utah Utes"
    community "Mountain West"
  ]
  node
  [
    id 74
    name "Wyoming Cowboys"
    community "Mountain West"
  ]
  node
  [
    id 75
    name "New Mexico Lobos"
    community "Mountain West"
  ]
  node
  [
    id 76
    name "Air Force Falcons"
    community "Mountain West"
  ]
  node
  [
    id 77
    name "San Diego State Aztecs"
    community "Mountain West"
  ]
  node
  [
    id 78
    name "Colorado State Rams"
    community "Mountain West"
  ]
  node
  [
    id 79
    name "UNLV Rebels"
    community "Mountain West"
  ]
  node
  [
    id 80
    name "USC Trojans"
    community "Pac-12"
  ]
  node
  [
    id 81
    name "California Golden Bears"
    community "Pac-12"
  ]
  node
  [
    id 82
    name "Oregon State Beavers"
    community "Pac-12"
  ]
  node
  [
    id 83
    name "UCLA Bruins"
    community "Pac-12"
  ]
  node
  [
    id 84
    name "Arizona State Sun Devils"
    community "Pac-12"
  ]
  node
  [
    id 85
    name "Oregon Ducks"
    community "Pac-12"
  ]
  node
  [
    id 86
    name "Arizona Wildcats"
    community "Pac-12"
  ]
  node
  [
    id 87
    name "Washington State Cougars"
    community "Pac-12"
  ]
  node
  [
    id 88
    name "Washington Huskies"
    community "Pac-12"
  ]
  node
  [
    id 89
    name "Stanford Cardinal"
    community "Pac-12"
  ]
  node
  [
    id 90
    name "Florida Gators"
    community "Southeastern"
  ]
  node
  [
    id 91
    name "Tennessee Volunteers"
    community "Southeastern"
  ]
  node
  [
    id 92
    name "Georgia Bulldogs"
    community "Southeastern"
  ]
  node
  [
    id 93
    name "Kentucky Wildcats"
    community "Southeastern"
  ]
  node
  [
    id 94
    name "South Carolina Gamecocks"
    community "Southeastern"
  ]
  node
  [
    id 95
    name "Vanderbilt Commodores"
    community "Southeastern"
  ]
  node
  [
    id 96
    name "Arkansas Razorbacks"
    community "Southeastern"
  ]
  node
  [
    id 97
    name "Auburn Tigers"
    community "Southeastern"
  ]
  node
  [
    id 98
    name "LSU Tigers"
    community "Southeastern"
  ]
  node
  [
    id 99
    name "Alabama Crimson Tide"
    community "Southeastern"
  ]
  node
  [
    id 100
    name "Ole Miss Rebels"
    community "Southeastern"
  ]
  node
  [
    id 101
    name "Mississippi State Bulldogs"
    community "Southeastern"
  ]
  node
  [
    id 102
    name "Troy Trojans"
    community "Sunbelt"
  ]
  node
  [
    id 103
    name "Middle Tennessee Blue Raiders"
    community "Sunbelt"
  ]
  node
  [
    id 104
    name "Arkansas State Red Wolves"
    community "Sunbelt"
  ]
  node
  [
    id 105
    name "Florida Atlantic Owls"
    community "Sunbelt"
  ]
  node
  [
    id 106
    name "Louisiana Ragin' Cajuns"
    community "Sunbelt"
  ]
  node
  [
    id 107
    name "Louisiana Monroe Warhawks"
    community "Sunbelt"
  ]
  node
  [
    id 108
    name "North Texas Mean Green"
    community "Sunbelt"
  ]
  node
  [
    id 109
    name "Florida Intl Golden Panthers"
    community "Sunbelt"
  ]
  node
  [
    id 110
    name "Boise State Broncos"
    community "Western Athletic"
  ]
  node
  [
    id 111
    name "Hawai'i Rainbow Warriors"
    community "Western Athletic"
  ]
  node
  [
    id 112
    name "San Jose State Spartans"
    community "Western Athletic"
  ]
  node
  [
    id 113
    name "Nevada Wolf Pack"
    community "Western Athletic"
  ]
  node
  [
    id 114
    name "Fresno State Bulldogs"
    community "Western Athletic"
  ]
  node
  [
    id 115
    name "Idaho Vandals"
    community "Western Athletic"
  ]
  node
  [
    id 116
    name "New Mexico State Aggies"
    community "Western Athletic"
  ]
  node
  [
    id 117
    name "Louisiana Tech Bulldogs"
    community "Western Athletic"
  ]
  node
  [
    id 118
    name "Utah State Aggies"
    community "Western Athletic"
  ]
  edge
  [
    source 0
    target 0
  ]
  edge
  [
    source 1
    target 0
  ]
  edge
  [
    source 2
    target 0
  ]
  edge
  [
    source 3
    target 0
  ]
  edge
  [
    source 4
    target 0
  ]
  edge
  [
    source 5
    target 0
  ]
  edge
  [
    source 6
    target 0
  ]
  edge
  [
    source 7
    target 0
  ]
  edge
  [
    source 10
    target 0
  ]
  edge
  [
    source 11
    target 0
  ]
  edge
  [
    source 24
    target 0
  ]
  edge
  [
    source 30
    target 0
  ]
  edge
  [
    source 31
    target 0
  ]
  edge
  [
    source 100
    target 0
  ]
  edge
  [
    source 1
    target 1
  ]
  edge
  [
    source 2
    target 1
  ]
  edge
  [
    source 3
    target 1
  ]
  edge
  [
    source 4
    target 1
  ]
  edge
  [
    source 5
    target 1
  ]
  edge
  [
    source 7
    target 1
  ]
  edge
  [
    source 9
    target 1
  ]
  edge
  [
    source 11
    target 1
  ]
  edge
  [
    source 55
    target 1
  ]
  edge
  [
    source 64
    target 1
  ]
  edge
  [
    source 65
    target 1
  ]
  edge
  [
    source 71
    target 1
  ]
  edge
  [
    source 2
    target 2
  ]
  edge
  [
    source 3
    target 2
  ]
  edge
  [
    source 4
    target 2
  ]
  edge
  [
    source 5
    target 2
  ]
  edge
  [
    source 6
    target 2
  ]
  edge
  [
    source 8
    target 2
  ]
  edge
  [
    source 9
    target 2
  ]
  edge
  [
    source 26
    target 2
  ]
  edge
  [
    source 36
    target 2
  ]
  edge
  [
    source 103
    target 2
  ]
  edge
  [
    source 109
    target 2
  ]
  edge
  [
    source 3
    target 3
  ]
  edge
  [
    source 4
    target 3
  ]
  edge
  [
    source 5
    target 3
  ]
  edge
  [
    source 6
    target 3
  ]
  edge
  [
    source 7
    target 3
  ]
  edge
  [
    source 10
    target 3
  ]
  edge
  [
    source 58
    target 3
  ]
  edge
  [
    source 93
    target 3
  ]
  edge
  [
    source 94
    target 3
  ]
  edge
  [
    source 105
    target 3
  ]
  edge
  [
    source 117
    target 3
  ]
  edge
  [
    source 4
    target 4
  ]
  edge
  [
    source 5
    target 4
  ]
  edge
  [
    source 8
    target 4
  ]
  edge
  [
    source 9
    target 4
  ]
  edge
  [
    source 11
    target 4
  ]
  edge
  [
    source 50
    target 4
  ]
  edge
  [
    source 66
    target 4
  ]
  edge
  [
    source 83
    target 4
  ]
  edge
  [
    source 90
    target 4
  ]
  edge
  [
    source 102
    target 4
  ]
  edge
  [
    source 5
    target 5
  ]
  edge
  [
    source 6
    target 5
  ]
  edge
  [
    source 8
    target 5
  ]
  edge
  [
    source 10
    target 5
  ]
  edge
  [
    source 43
    target 5
  ]
  edge
  [
    source 44
    target 5
  ]
  edge
  [
    source 61
    target 5
  ]
  edge
  [
    source 6
    target 6
  ]
  edge
  [
    source 7
    target 6
  ]
  edge
  [
    source 8
    target 6
  ]
  edge
  [
    source 9
    target 6
  ]
  edge
  [
    source 10
    target 6
  ]
  edge
  [
    source 11
    target 6
  ]
  edge
  [
    source 26
    target 6
  ]
  edge
  [
    source 57
    target 6
  ]
  edge
  [
    source 92
    target 6
  ]
  edge
  [
    source 102
    target 6
  ]
  edge
  [
    source 7
    target 7
  ]
  edge
  [
    source 8
    target 7
  ]
  edge
  [
    source 9
    target 7
  ]
  edge
  [
    source 10
    target 7
  ]
  edge
  [
    source 11
    target 7
  ]
  edge
  [
    source 28
    target 7
  ]
  edge
  [
    source 43
    target 7
  ]
  edge
  [
    source 60
    target 7
  ]
  edge
  [
    source 92
    target 7
  ]
  edge
  [
    source 8
    target 8
  ]
  edge
  [
    source 9
    target 8
  ]
  edge
  [
    source 10
    target 8
  ]
  edge
  [
    source 11
    target 8
  ]
  edge
  [
    source 29
    target 8
  ]
  edge
  [
    source 44
    target 8
  ]
  edge
  [
    source 66
    target 8
  ]
  edge
  [
    source 74
    target 8
  ]
  edge
  [
    source 9
    target 9
  ]
  edge
  [
    source 10
    target 9
  ]
  edge
  [
    source 11
    target 9
  ]
  edge
  [
    source 24
    target 9
  ]
  edge
  [
    source 49
    target 9
  ]
  edge
  [
    source 109
    target 9
  ]
  edge
  [
    source 113
    target 9
  ]
  edge
  [
    source 10
    target 10
  ]
  edge
  [
    source 11
    target 10
  ]
  edge
  [
    source 25
    target 10
  ]
  edge
  [
    source 27
    target 10
  ]
  edge
  [
    source 57
    target 10
  ]
  edge
  [
    source 11
    target 11
  ]
  edge
  [
    source 55
    target 11
  ]
  edge
  [
    source 95
    target 11
  ]
  edge
  [
    source 99
    target 11
  ]
  edge
  [
    source 12
    target 12
  ]
  edge
  [
    source 13
    target 12
  ]
  edge
  [
    source 14
    target 12
  ]
  edge
  [
    source 15
    target 12
  ]
  edge
  [
    source 16
    target 12
  ]
  edge
  [
    source 17
    target 12
  ]
  edge
  [
    source 18
    target 12
  ]
  edge
  [
    source 19
    target 12
  ]
  edge
  [
    source 20
    target 12
  ]
  edge
  [
    source 22
    target 12
  ]
  edge
  [
    source 80
    target 12
  ]
  edge
  [
    source 97
    target 12
  ]
  edge
  [
    source 102
    target 12
  ]
  edge
  [
    source 117
    target 12
  ]
  edge
  [
    source 13
    target 13
  ]
  edge
  [
    source 14
    target 13
  ]
  edge
  [
    source 15
    target 13
  ]
  edge
  [
    source 16
    target 13
  ]
  edge
  [
    source 17
    target 13
  ]
  edge
  [
    source 18
    target 13
  ]
  edge
  [
    source 20
    target 13
  ]
  edge
  [
    source 21
    target 13
  ]
  edge
  [
    source 59
    target 13
  ]
  edge
  [
    source 75
    target 13
  ]
  edge
  [
    source 82
    target 13
  ]
  edge
  [
    source 100
    target 13
  ]
  edge
  [
    source 14
    target 14
  ]
  edge
  [
    source 15
    target 14
  ]
  edge
  [
    source 16
    target 14
  ]
  edge
  [
    source 17
    target 14
  ]
  edge
  [
    source 19
    target 14
  ]
  edge
  [
    source 22
    target 14
  ]
  edge
  [
    source 23
    target 14
  ]
  edge
  [
    source 24
    target 14
  ]
  edge
  [
    source 25
    target 14
  ]
  edge
  [
    source 45
    target 14
  ]
  edge
  [
    source 105
    target 14
  ]
  edge
  [
    source 15
    target 15
  ]
  edge
  [
    source 16
    target 15
  ]
  edge
  [
    source 17
    target 15
  ]
  edge
  [
    source 20
    target 15
  ]
  edge
  [
    source 22
    target 15
  ]
  edge
  [
    source 23
    target 15
  ]
  edge
  [
    source 27
    target 15
  ]
  edge
  [
    source 69
    target 15
  ]
  edge
  [
    source 107
    target 15
  ]
  edge
  [
    source 16
    target 16
  ]
  edge
  [
    source 17
    target 16
  ]
  edge
  [
    source 18
    target 16
  ]
  edge
  [
    source 21
    target 16
  ]
  edge
  [
    source 23
    target 16
  ]
  edge
  [
    source 78
    target 16
  ]
  edge
  [
    source 84
    target 16
  ]
  edge
  [
    source 92
    target 16
  ]
  edge
  [
    source 17
    target 17
  ]
  edge
  [
    source 18
    target 17
  ]
  edge
  [
    source 19
    target 17
  ]
  edge
  [
    source 21
    target 17
  ]
  edge
  [
    source 39
    target 17
  ]
  edge
  [
    source 69
    target 17
  ]
  edge
  [
    source 79
    target 17
  ]
  edge
  [
    source 18
    target 18
  ]
  edge
  [
    source 19
    target 18
  ]
  edge
  [
    source 20
    target 18
  ]
  edge
  [
    source 21
    target 18
  ]
  edge
  [
    source 22
    target 18
  ]
  edge
  [
    source 23
    target 18
  ]
  edge
  [
    source 47
    target 18
  ]
  edge
  [
    source 85
    target 18
  ]
  edge
  [
    source 88
    target 18
  ]
  edge
  [
    source 103
    target 18
  ]
  edge
  [
    source 110
    target 18
  ]
  edge
  [
    source 19
    target 19
  ]
  edge
  [
    source 20
    target 19
  ]
  edge
  [
    source 21
    target 19
  ]
  edge
  [
    source 22
    target 19
  ]
  edge
  [
    source 23
    target 19
  ]
  edge
  [
    source 32
    target 19
  ]
  edge
  [
    source 39
    target 19
  ]
  edge
  [
    source 50
    target 19
  ]
  edge
  [
    source 108
    target 19
  ]
  edge
  [
    source 20
    target 20
  ]
  edge
  [
    source 21
    target 20
  ]
  edge
  [
    source 22
    target 20
  ]
  edge
  [
    source 23
    target 20
  ]
  edge
  [
    source 56
    target 20
  ]
  edge
  [
    source 81
    target 20
  ]
  edge
  [
    source 106
    target 20
  ]
  edge
  [
    source 117
    target 20
  ]
  edge
  [
    source 21
    target 21
  ]
  edge
  [
    source 22
    target 21
  ]
  edge
  [
    source 23
    target 21
  ]
  edge
  [
    source 37
    target 21
  ]
  edge
  [
    source 52
    target 21
  ]
  edge
  [
    source 53
    target 21
  ]
  edge
  [
    source 72
    target 21
  ]
  edge
  [
    source 22
    target 22
  ]
  edge
  [
    source 23
    target 22
  ]
  edge
  [
    source 49
    target 22
  ]
  edge
  [
    source 99
    target 22
  ]
  edge
  [
    source 104
    target 22
  ]
  edge
  [
    source 105
    target 22
  ]
  edge
  [
    source 23
    target 23
  ]
  edge
  [
    source 56
    target 23
  ]
  edge
  [
    source 72
    target 23
  ]
  edge
  [
    source 87
    target 23
  ]
  edge
  [
    source 24
    target 24
  ]
  edge
  [
    source 25
    target 24
  ]
  edge
  [
    source 26
    target 24
  ]
  edge
  [
    source 27
    target 24
  ]
  edge
  [
    source 28
    target 24
  ]
  edge
  [
    source 29
    target 24
  ]
  edge
  [
    source 30
    target 24
  ]
  edge
  [
    source 31
    target 24
  ]
  edge
  [
    source 58
    target 24
  ]
  edge
  [
    source 93
    target 24
  ]
  edge
  [
    source 103
    target 24
  ]
  edge
  [
    source 25
    target 25
  ]
  edge
  [
    source 26
    target 25
  ]
  edge
  [
    source 27
    target 25
  ]
  edge
  [
    source 28
    target 25
  ]
  edge
  [
    source 29
    target 25
  ]
  edge
  [
    source 30
    target 25
  ]
  edge
  [
    source 31
    target 25
  ]
  edge
  [
    source 42
    target 25
  ]
  edge
  [
    source 55
    target 25
  ]
  edge
  [
    source 59
    target 25
  ]
  edge
  [
    source 26
    target 26
  ]
  edge
  [
    source 27
    target 26
  ]
  edge
  [
    source 28
    target 26
  ]
  edge
  [
    source 29
    target 26
  ]
  edge
  [
    source 30
    target 26
  ]
  edge
  [
    source 31
    target 26
  ]
  edge
  [
    source 44
    target 26
  ]
  edge
  [
    source 45
    target 26
  ]
  edge
  [
    source 101
    target 26
  ]
  edge
  [
    source 27
    target 27
  ]
  edge
  [
    source 28
    target 27
  ]
  edge
  [
    source 29
    target 27
  ]
  edge
  [
    source 30
    target 27
  ]
  edge
  [
    source 31
    target 27
  ]
  edge
  [
    source 44
    target 27
  ]
  edge
  [
    source 46
    target 27
  ]
  edge
  [
    source 109
    target 27
  ]
  edge
  [
    source 28
    target 28
  ]
  edge
  [
    source 29
    target 28
  ]
  edge
  [
    source 30
    target 28
  ]
  edge
  [
    source 31
    target 28
  ]
  edge
  [
    source 32
    target 28
  ]
  edge
  [
    source 61
    target 28
  ]
  edge
  [
    source 63
    target 28
  ]
  edge
  [
    source 66
    target 28
  ]
  edge
  [
    source 29
    target 29
  ]
  edge
  [
    source 30
    target 29
  ]
  edge
  [
    source 31
    target 29
  ]
  edge
  [
    source 41
    target 29
  ]
  edge
  [
    source 46
    target 29
  ]
  edge
  [
    source 69
    target 29
  ]
  edge
  [
    source 30
    target 30
  ]
  edge
  [
    source 31
    target 30
  ]
  edge
  [
    source 38
    target 30
  ]
  edge
  [
    source 55
    target 30
  ]
  edge
  [
    source 56
    target 30
  ]
  edge
  [
    source 31
    target 31
  ]
  edge
  [
    source 39
    target 31
  ]
  edge
  [
    source 42
    target 31
  ]
  edge
  [
    source 63
    target 31
  ]
  edge
  [
    source 74
    target 31
  ]
  edge
  [
    source 32
    target 32
  ]
  edge
  [
    source 34
    target 32
  ]
  edge
  [
    source 35
    target 32
  ]
  edge
  [
    source 37
    target 32
  ]
  edge
  [
    source 38
    target 32
  ]
  edge
  [
    source 39
    target 32
  ]
  edge
  [
    source 40
    target 32
  ]
  edge
  [
    source 41
    target 32
  ]
  edge
  [
    source 42
    target 32
  ]
  edge
  [
    source 62
    target 32
  ]
  edge
  [
    source 67
    target 32
  ]
  edge
  [
    source 90
    target 32
  ]
  edge
  [
    source 33
    target 33
  ]
  edge
  [
    source 34
    target 33
  ]
  edge
  [
    source 35
    target 33
  ]
  edge
  [
    source 36
    target 33
  ]
  edge
  [
    source 37
    target 33
  ]
  edge
  [
    source 38
    target 33
  ]
  edge
  [
    source 39
    target 33
  ]
  edge
  [
    source 40
    target 33
  ]
  edge
  [
    source 42
    target 33
  ]
  edge
  [
    source 62
    target 33
  ]
  edge
  [
    source 64
    target 33
  ]
  edge
  [
    source 77
    target 33
  ]
  edge
  [
    source 96
    target 33
  ]
  edge
  [
    source 34
    target 34
  ]
  edge
  [
    source 35
    target 34
  ]
  edge
  [
    source 37
    target 34
  ]
  edge
  [
    source 38
    target 34
  ]
  edge
  [
    source 39
    target 34
  ]
  edge
  [
    source 40
    target 34
  ]
  edge
  [
    source 41
    target 34
  ]
  edge
  [
    source 57
    target 34
  ]
  edge
  [
    source 65
    target 34
  ]
  edge
  [
    source 68
    target 34
  ]
  edge
  [
    source 80
    target 34
  ]
  edge
  [
    source 95
    target 34
  ]
  edge
  [
    source 35
    target 35
  ]
  edge
  [
    source 36
    target 35
  ]
  edge
  [
    source 37
    target 35
  ]
  edge
  [
    source 40
    target 35
  ]
  edge
  [
    source 41
    target 35
  ]
  edge
  [
    source 42
    target 35
  ]
  edge
  [
    source 57
    target 35
  ]
  edge
  [
    source 58
    target 35
  ]
  edge
  [
    source 61
    target 35
  ]
  edge
  [
    source 91
    target 35
  ]
  edge
  [
    source 36
    target 36
  ]
  edge
  [
    source 37
    target 36
  ]
  edge
  [
    source 38
    target 36
  ]
  edge
  [
    source 39
    target 36
  ]
  edge
  [
    source 40
    target 36
  ]
  edge
  [
    source 41
    target 36
  ]
  edge
  [
    source 42
    target 36
  ]
  edge
  [
    source 57
    target 36
  ]
  edge
  [
    source 63
    target 36
  ]
  edge
  [
    source 68
    target 36
  ]
  edge
  [
    source 111
    target 36
  ]
  edge
  [
    source 37
    target 37
  ]
  edge
  [
    source 38
    target 37
  ]
  edge
  [
    source 39
    target 37
  ]
  edge
  [
    source 41
    target 37
  ]
  edge
  [
    source 58
    target 37
  ]
  edge
  [
    source 60
    target 37
  ]
  edge
  [
    source 81
    target 37
  ]
  edge
  [
    source 38
    target 38
  ]
  edge
  [
    source 39
    target 38
  ]
  edge
  [
    source 41
    target 38
  ]
  edge
  [
    source 42
    target 38
  ]
  edge
  [
    source 66
    target 38
  ]
  edge
  [
    source 68
    target 38
  ]
  edge
  [
    source 39
    target 39
  ]
  edge
  [
    source 40
    target 39
  ]
  edge
  [
    source 42
    target 39
  ]
  edge
  [
    source 67
    target 39
  ]
  edge
  [
    source 40
    target 40
  ]
  edge
  [
    source 41
    target 40
  ]
  edge
  [
    source 42
    target 40
  ]
  edge
  [
    source 63
    target 40
  ]
  edge
  [
    source 70
    target 40
  ]
  edge
  [
    source 113
    target 40
  ]
  edge
  [
    source 41
    target 41
  ]
  edge
  [
    source 42
    target 41
  ]
  edge
  [
    source 57
    target 41
  ]
  edge
  [
    source 70
    target 41
  ]
  edge
  [
    source 115
    target 41
  ]
  edge
  [
    source 42
    target 42
  ]
  edge
  [
    source 59
    target 42
  ]
  edge
  [
    source 43
    target 43
  ]
  edge
  [
    source 44
    target 43
  ]
  edge
  [
    source 45
    target 43
  ]
  edge
  [
    source 46
    target 43
  ]
  edge
  [
    source 47
    target 43
  ]
  edge
  [
    source 48
    target 43
  ]
  edge
  [
    source 49
    target 43
  ]
  edge
  [
    source 51
    target 43
  ]
  edge
  [
    source 54
    target 43
  ]
  edge
  [
    source 59
    target 43
  ]
  edge
  [
    source 90
    target 43
  ]
  edge
  [
    source 44
    target 44
  ]
  edge
  [
    source 45
    target 44
  ]
  edge
  [
    source 46
    target 44
  ]
  edge
  [
    source 47
    target 44
  ]
  edge
  [
    source 48
    target 44
  ]
  edge
  [
    source 50
    target 44
  ]
  edge
  [
    source 51
    target 44
  ]
  edge
  [
    source 52
    target 44
  ]
  edge
  [
    source 55
    target 44
  ]
  edge
  [
    source 45
    target 45
  ]
  edge
  [
    source 46
    target 45
  ]
  edge
  [
    source 47
    target 45
  ]
  edge
  [
    source 48
    target 45
  ]
  edge
  [
    source 52
    target 45
  ]
  edge
  [
    source 53
    target 45
  ]
  edge
  [
    source 54
    target 45
  ]
  edge
  [
    source 91
    target 45
  ]
  edge
  [
    source 46
    target 46
  ]
  edge
  [
    source 47
    target 46
  ]
  edge
  [
    source 48
    target 46
  ]
  edge
  [
    source 49
    target 46
  ]
  edge
  [
    source 50
    target 46
  ]
  edge
  [
    source 54
    target 46
  ]
  edge
  [
    source 90
    target 46
  ]
  edge
  [
    source 47
    target 47
  ]
  edge
  [
    source 48
    target 47
  ]
  edge
  [
    source 50
    target 47
  ]
  edge
  [
    source 52
    target 47
  ]
  edge
  [
    source 53
    target 47
  ]
  edge
  [
    source 92
    target 47
  ]
  edge
  [
    source 101
    target 47
  ]
  edge
  [
    source 102
    target 47
  ]
  edge
  [
    source 48
    target 48
  ]
  edge
  [
    source 49
    target 48
  ]
  edge
  [
    source 51
    target 48
  ]
  edge
  [
    source 53
    target 48
  ]
  edge
  [
    source 91
    target 48
  ]
  edge
  [
    source 100
    target 48
  ]
  edge
  [
    source 104
    target 48
  ]
  edge
  [
    source 49
    target 49
  ]
  edge
  [
    source 50
    target 49
  ]
  edge
  [
    source 51
    target 49
  ]
  edge
  [
    source 52
    target 49
  ]
  edge
  [
    source 53
    target 49
  ]
  edge
  [
    source 54
    target 49
  ]
  edge
  [
    source 94
    target 49
  ]
  edge
  [
    source 106
    target 49
  ]
  edge
  [
    source 50
    target 50
  ]
  edge
  [
    source 51
    target 50
  ]
  edge
  [
    source 52
    target 50
  ]
  edge
  [
    source 53
    target 50
  ]
  edge
  [
    source 54
    target 50
  ]
  edge
  [
    source 56
    target 50
  ]
  edge
  [
    source 83
    target 50
  ]
  edge
  [
    source 102
    target 50
  ]
  edge
  [
    source 51
    target 51
  ]
  edge
  [
    source 52
    target 51
  ]
  edge
  [
    source 53
    target 51
  ]
  edge
  [
    source 54
    target 51
  ]
  edge
  [
    source 55
    target 51
  ]
  edge
  [
    source 71
    target 51
  ]
  edge
  [
    source 73
    target 51
  ]
  edge
  [
    source 108
    target 51
  ]
  edge
  [
    source 52
    target 52
  ]
  edge
  [
    source 53
    target 52
  ]
  edge
  [
    source 54
    target 52
  ]
  edge
  [
    source 104
    target 52
  ]
  edge
  [
    source 108
    target 52
  ]
  edge
  [
    source 53
    target 53
  ]
  edge
  [
    source 54
    target 53
  ]
  edge
  [
    source 75
    target 53
  ]
  edge
  [
    source 77
    target 53
  ]
  edge
  [
    source 116
    target 53
  ]
  edge
  [
    source 54
    target 54
  ]
  edge
  [
    source 56
    target 54
  ]
  edge
  [
    source 97
    target 54
  ]
  edge
  [
    source 98
    target 54
  ]
  edge
  [
    source 101
    target 54
  ]
  edge
  [
    source 55
    target 55
  ]
  edge
  [
    source 56
    target 55
  ]
  edge
  [
    source 57
    target 55
  ]
  edge
  [
    source 58
    target 55
  ]
  edge
  [
    source 70
    target 55
  ]
  edge
  [
    source 76
    target 55
  ]
  edge
  [
    source 89
    target 55
  ]
  edge
  [
    source 56
    target 56
  ]
  edge
  [
    source 57
    target 56
  ]
  edge
  [
    source 60
    target 56
  ]
  edge
  [
    source 72
    target 56
  ]
  edge
  [
    source 76
    target 56
  ]
  edge
  [
    source 104
    target 56
  ]
  edge
  [
    source 57
    target 57
  ]
  edge
  [
    source 76
    target 57
  ]
  edge
  [
    source 80
    target 57
  ]
  edge
  [
    source 83
    target 57
  ]
  edge
  [
    source 89
    target 57
  ]
  edge
  [
    source 98
    target 57
  ]
  edge
  [
    source 58
    target 58
  ]
  edge
  [
    source 60
    target 58
  ]
  edge
  [
    source 62
    target 58
  ]
  edge
  [
    source 64
    target 58
  ]
  edge
  [
    source 65
    target 58
  ]
  edge
  [
    source 66
    target 58
  ]
  edge
  [
    source 67
    target 58
  ]
  edge
  [
    source 95
    target 58
  ]
  edge
  [
    source 59
    target 59
  ]
  edge
  [
    source 60
    target 59
  ]
  edge
  [
    source 61
    target 59
  ]
  edge
  [
    source 62
    target 59
  ]
  edge
  [
    source 63
    target 59
  ]
  edge
  [
    source 64
    target 59
  ]
  edge
  [
    source 65
    target 59
  ]
  edge
  [
    source 66
    target 59
  ]
  edge
  [
    source 67
    target 59
  ]
  edge
  [
    source 70
    target 59
  ]
  edge
  [
    source 60
    target 60
  ]
  edge
  [
    source 61
    target 60
  ]
  edge
  [
    source 62
    target 60
  ]
  edge
  [
    source 63
    target 60
  ]
  edge
  [
    source 64
    target 60
  ]
  edge
  [
    source 68
    target 60
  ]
  edge
  [
    source 69
    target 60
  ]
  edge
  [
    source 70
    target 60
  ]
  edge
  [
    source 61
    target 61
  ]
  edge
  [
    source 62
    target 61
  ]
  edge
  [
    source 63
    target 61
  ]
  edge
  [
    source 64
    target 61
  ]
  edge
  [
    source 65
    target 61
  ]
  edge
  [
    source 66
    target 61
  ]
  edge
  [
    source 69
    target 61
  ]
  edge
  [
    source 108
    target 61
  ]
  edge
  [
    source 62
    target 62
  ]
  edge
  [
    source 63
    target 62
  ]
  edge
  [
    source 64
    target 62
  ]
  edge
  [
    source 65
    target 62
  ]
  edge
  [
    source 69
    target 62
  ]
  edge
  [
    source 70
    target 62
  ]
  edge
  [
    source 109
    target 62
  ]
  edge
  [
    source 63
    target 63
  ]
  edge
  [
    source 64
    target 63
  ]
  edge
  [
    source 66
    target 63
  ]
  edge
  [
    source 67
    target 63
  ]
  edge
  [
    source 68
    target 63
  ]
  edge
  [
    source 64
    target 64
  ]
  edge
  [
    source 65
    target 64
  ]
  edge
  [
    source 67
    target 64
  ]
  edge
  [
    source 68
    target 64
  ]
  edge
  [
    source 97
    target 64
  ]
  edge
  [
    source 65
    target 65
  ]
  edge
  [
    source 66
    target 65
  ]
  edge
  [
    source 67
    target 65
  ]
  edge
  [
    source 68
    target 65
  ]
  edge
  [
    source 69
    target 65
  ]
  edge
  [
    source 70
    target 65
  ]
  edge
  [
    source 93
    target 65
  ]
  edge
  [
    source 103
    target 65
  ]
  edge
  [
    source 66
    target 66
  ]
  edge
  [
    source 67
    target 66
  ]
  edge
  [
    source 68
    target 66
  ]
  edge
  [
    source 69
    target 66
  ]
  edge
  [
    source 70
    target 66
  ]
  edge
  [
    source 67
    target 67
  ]
  edge
  [
    source 68
    target 67
  ]
  edge
  [
    source 69
    target 67
  ]
  edge
  [
    source 70
    target 67
  ]
  edge
  [
    source 72
    target 67
  ]
  edge
  [
    source 68
    target 68
  ]
  edge
  [
    source 69
    target 68
  ]
  edge
  [
    source 70
    target 68
  ]
  edge
  [
    source 69
    target 69
  ]
  edge
  [
    source 70
    target 69
  ]
  edge
  [
    source 70
    target 70
  ]
  edge
  [
    source 106
    target 70
  ]
  edge
  [
    source 71
    target 71
  ]
  edge
  [
    source 72
    target 71
  ]
  edge
  [
    source 73
    target 71
  ]
  edge
  [
    source 74
    target 71
  ]
  edge
  [
    source 75
    target 71
  ]
  edge
  [
    source 76
    target 71
  ]
  edge
  [
    source 77
    target 71
  ]
  edge
  [
    source 78
    target 71
  ]
  edge
  [
    source 79
    target 71
  ]
  edge
  [
    source 85
    target 71
  ]
  edge
  [
    source 86
    target 71
  ]
  edge
  [
    source 118
    target 71
  ]
  edge
  [
    source 72
    target 72
  ]
  edge
  [
    source 73
    target 72
  ]
  edge
  [
    source 74
    target 72
  ]
  edge
  [
    source 75
    target 72
  ]
  edge
  [
    source 76
    target 72
  ]
  edge
  [
    source 77
    target 72
  ]
  edge
  [
    source 78
    target 72
  ]
  edge
  [
    source 79
    target 72
  ]
  edge
  [
    source 73
    target 73
  ]
  edge
  [
    source 74
    target 73
  ]
  edge
  [
    source 75
    target 73
  ]
  edge
  [
    source 76
    target 73
  ]
  edge
  [
    source 77
    target 73
  ]
  edge
  [
    source 78
    target 73
  ]
  edge
  [
    source 79
    target 73
  ]
  edge
  [
    source 83
    target 73
  ]
  edge
  [
    source 110
    target 73
  ]
  edge
  [
    source 118
    target 73
  ]
  edge
  [
    source 74
    target 74
  ]
  edge
  [
    source 75
    target 74
  ]
  edge
  [
    source 76
    target 74
  ]
  edge
  [
    source 77
    target 74
  ]
  edge
  [
    source 78
    target 74
  ]
  edge
  [
    source 79
    target 74
  ]
  edge
  [
    source 110
    target 74
  ]
  edge
  [
    source 118
    target 74
  ]
  edge
  [
    source 75
    target 75
  ]
  edge
  [
    source 76
    target 75
  ]
  edge
  [
    source 77
    target 75
  ]
  edge
  [
    source 78
    target 75
  ]
  edge
  [
    source 79
    target 75
  ]
  edge
  [
    source 112
    target 75
  ]
  edge
  [
    source 116
    target 75
  ]
  edge
  [
    source 76
    target 76
  ]
  edge
  [
    source 77
    target 76
  ]
  edge
  [
    source 78
    target 76
  ]
  edge
  [
    source 79
    target 76
  ]
  edge
  [
    source 91
    target 76
  ]
  edge
  [
    source 77
    target 77
  ]
  edge
  [
    source 78
    target 77
  ]
  edge
  [
    source 79
    target 77
  ]
  edge
  [
    source 112
    target 77
  ]
  edge
  [
    source 78
    target 78
  ]
  edge
  [
    source 79
    target 78
  ]
  edge
  [
    source 113
    target 78
  ]
  edge
  [
    source 114
    target 78
  ]
  edge
  [
    source 79
    target 79
  ]
  edge
  [
    source 113
    target 79
  ]
  edge
  [
    source 80
    target 80
  ]
  edge
  [
    source 81
    target 80
  ]
  edge
  [
    source 82
    target 80
  ]
  edge
  [
    source 83
    target 80
  ]
  edge
  [
    source 84
    target 80
  ]
  edge
  [
    source 85
    target 80
  ]
  edge
  [
    source 86
    target 80
  ]
  edge
  [
    source 87
    target 80
  ]
  edge
  [
    source 88
    target 80
  ]
  edge
  [
    source 89
    target 80
  ]
  edge
  [
    source 96
    target 80
  ]
  edge
  [
    source 81
    target 81
  ]
  edge
  [
    source 82
    target 81
  ]
  edge
  [
    source 83
    target 81
  ]
  edge
  [
    source 84
    target 81
  ]
  edge
  [
    source 85
    target 81
  ]
  edge
  [
    source 86
    target 81
  ]
  edge
  [
    source 87
    target 81
  ]
  edge
  [
    source 88
    target 81
  ]
  edge
  [
    source 89
    target 81
  ]
  edge
  [
    source 91
    target 81
  ]
  edge
  [
    source 82
    target 82
  ]
  edge
  [
    source 83
    target 82
  ]
  edge
  [
    source 84
    target 82
  ]
  edge
  [
    source 85
    target 82
  ]
  edge
  [
    source 86
    target 82
  ]
  edge
  [
    source 87
    target 82
  ]
  edge
  [
    source 88
    target 82
  ]
  edge
  [
    source 89
    target 82
  ]
  edge
  [
    source 110
    target 82
  ]
  edge
  [
    source 111
    target 82
  ]
  edge
  [
    source 115
    target 82
  ]
  edge
  [
    source 83
    target 83
  ]
  edge
  [
    source 84
    target 83
  ]
  edge
  [
    source 85
    target 83
  ]
  edge
  [
    source 86
    target 83
  ]
  edge
  [
    source 87
    target 83
  ]
  edge
  [
    source 88
    target 83
  ]
  edge
  [
    source 89
    target 83
  ]
  edge
  [
    source 84
    target 84
  ]
  edge
  [
    source 85
    target 84
  ]
  edge
  [
    source 86
    target 84
  ]
  edge
  [
    source 87
    target 84
  ]
  edge
  [
    source 88
    target 84
  ]
  edge
  [
    source 89
    target 84
  ]
  edge
  [
    source 111
    target 84
  ]
  edge
  [
    source 113
    target 84
  ]
  edge
  [
    source 85
    target 85
  ]
  edge
  [
    source 86
    target 85
  ]
  edge
  [
    source 87
    target 85
  ]
  edge
  [
    source 88
    target 85
  ]
  edge
  [
    source 89
    target 85
  ]
  edge
  [
    source 114
    target 85
  ]
  edge
  [
    source 86
    target 86
  ]
  edge
  [
    source 87
    target 86
  ]
  edge
  [
    source 88
    target 86
  ]
  edge
  [
    source 89
    target 86
  ]
  edge
  [
    source 98
    target 86
  ]
  edge
  [
    source 87
    target 87
  ]
  edge
  [
    source 88
    target 87
  ]
  edge
  [
    source 89
    target 87
  ]
  edge
  [
    source 97
    target 87
  ]
  edge
  [
    source 115
    target 87
  ]
  edge
  [
    source 88
    target 88
  ]
  edge
  [
    source 89
    target 88
  ]
  edge
  [
    source 112
    target 88
  ]
  edge
  [
    source 114
    target 88
  ]
  edge
  [
    source 89
    target 89
  ]
  edge
  [
    source 112
    target 89
  ]
  edge
  [
    source 90
    target 90
  ]
  edge
  [
    source 91
    target 90
  ]
  edge
  [
    source 92
    target 90
  ]
  edge
  [
    source 93
    target 90
  ]
  edge
  [
    source 94
    target 90
  ]
  edge
  [
    source 95
    target 90
  ]
  edge
  [
    source 96
    target 90
  ]
  edge
  [
    source 97
    target 90
  ]
  edge
  [
    source 98
    target 90
  ]
  edge
  [
    source 99
    target 90
  ]
  edge
  [
    source 91
    target 91
  ]
  edge
  [
    source 92
    target 91
  ]
  edge
  [
    source 93
    target 91
  ]
  edge
  [
    source 94
    target 91
  ]
  edge
  [
    source 95
    target 91
  ]
  edge
  [
    source 96
    target 91
  ]
  edge
  [
    source 98
    target 91
  ]
  edge
  [
    source 99
    target 91
  ]
  edge
  [
    source 92
    target 92
  ]
  edge
  [
    source 93
    target 92
  ]
  edge
  [
    source 94
    target 92
  ]
  edge
  [
    source 95
    target 92
  ]
  edge
  [
    source 97
    target 92
  ]
  edge
  [
    source 100
    target 92
  ]
  edge
  [
    source 101
    target 92
  ]
  edge
  [
    source 93
    target 93
  ]
  edge
  [
    source 94
    target 93
  ]
  edge
  [
    source 95
    target 93
  ]
  edge
  [
    source 98
    target 93
  ]
  edge
  [
    source 100
    target 93
  ]
  edge
  [
    source 101
    target 93
  ]
  edge
  [
    source 107
    target 93
  ]
  edge
  [
    source 94
    target 94
  ]
  edge
  [
    source 95
    target 94
  ]
  edge
  [
    source 96
    target 94
  ]
  edge
  [
    source 97
    target 94
  ]
  edge
  [
    source 101
    target 94
  ]
  edge
  [
    source 103
    target 94
  ]
  edge
  [
    source 105
    target 94
  ]
  edge
  [
    source 95
    target 95
  ]
  edge
  [
    source 96
    target 95
  ]
  edge
  [
    source 99
    target 95
  ]
  edge
  [
    source 100
    target 95
  ]
  edge
  [
    source 96
    target 96
  ]
  edge
  [
    source 97
    target 96
  ]
  edge
  [
    source 98
    target 96
  ]
  edge
  [
    source 99
    target 96
  ]
  edge
  [
    source 100
    target 96
  ]
  edge
  [
    source 101
    target 96
  ]
  edge
  [
    source 107
    target 96
  ]
  edge
  [
    source 118
    target 96
  ]
  edge
  [
    source 97
    target 97
  ]
  edge
  [
    source 98
    target 97
  ]
  edge
  [
    source 99
    target 97
  ]
  edge
  [
    source 100
    target 97
  ]
  edge
  [
    source 101
    target 97
  ]
  edge
  [
    source 104
    target 97
  ]
  edge
  [
    source 98
    target 98
  ]
  edge
  [
    source 99
    target 98
  ]
  edge
  [
    source 100
    target 98
  ]
  edge
  [
    source 101
    target 98
  ]
  edge
  [
    source 106
    target 98
  ]
  edge
  [
    source 114
    target 98
  ]
  edge
  [
    source 99
    target 99
  ]
  edge
  [
    source 100
    target 99
  ]
  edge
  [
    source 101
    target 99
  ]
  edge
  [
    source 107
    target 99
  ]
  edge
  [
    source 109
    target 99
  ]
  edge
  [
    source 111
    target 99
  ]
  edge
  [
    source 100
    target 100
  ]
  edge
  [
    source 101
    target 100
  ]
  edge
  [
    source 101
    target 101
  ]
  edge
  [
    source 102
    target 102
  ]
  edge
  [
    source 103
    target 102
  ]
  edge
  [
    source 104
    target 102
  ]
  edge
  [
    source 105
    target 102
  ]
  edge
  [
    source 106
    target 102
  ]
  edge
  [
    source 107
    target 102
  ]
  edge
  [
    source 108
    target 102
  ]
  edge
  [
    source 109
    target 102
  ]
  edge
  [
    source 103
    target 103
  ]
  edge
  [
    source 104
    target 103
  ]
  edge
  [
    source 105
    target 103
  ]
  edge
  [
    source 106
    target 103
  ]
  edge
  [
    source 107
    target 103
  ]
  edge
  [
    source 108
    target 103
  ]
  edge
  [
    source 109
    target 103
  ]
  edge
  [
    source 104
    target 104
  ]
  edge
  [
    source 105
    target 104
  ]
  edge
  [
    source 106
    target 104
  ]
  edge
  [
    source 107
    target 104
  ]
  edge
  [
    source 108
    target 104
  ]
  edge
  [
    source 109
    target 104
  ]
  edge
  [
    source 105
    target 105
  ]
  edge
  [
    source 106
    target 105
  ]
  edge
  [
    source 107
    target 105
  ]
  edge
  [
    source 108
    target 105
  ]
  edge
  [
    source 109
    target 105
  ]
  edge
  [
    source 106
    target 106
  ]
  edge
  [
    source 107
    target 106
  ]
  edge
  [
    source 108
    target 106
  ]
  edge
  [
    source 109
    target 106
  ]
  edge
  [
    source 107
    target 107
  ]
  edge
  [
    source 108
    target 107
  ]
  edge
  [
    source 109
    target 107
  ]
  edge
  [
    source 108
    target 108
  ]
  edge
  [
    source 109
    target 108
  ]
  edge
  [
    source 117
    target 108
  ]
  edge
  [
    source 109
    target 109
  ]
  edge
  [
    source 110
    target 110
  ]
  edge
  [
    source 111
    target 110
  ]
  edge
  [
    source 112
    target 110
  ]
  edge
  [
    source 113
    target 110
  ]
  edge
  [
    source 114
    target 110
  ]
  edge
  [
    source 115
    target 110
  ]
  edge
  [
    source 116
    target 110
  ]
  edge
  [
    source 117
    target 110
  ]
  edge
  [
    source 118
    target 110
  ]
  edge
  [
    source 111
    target 111
  ]
  edge
  [
    source 112
    target 111
  ]
  edge
  [
    source 114
    target 111
  ]
  edge
  [
    source 116
    target 111
  ]
  edge
  [
    source 117
    target 111
  ]
  edge
  [
    source 118
    target 111
  ]
  edge
  [
    source 112
    target 112
  ]
  edge
  [
    source 113
    target 112
  ]
  edge
  [
    source 114
    target 112
  ]
  edge
  [
    source 115
    target 112
  ]
  edge
  [
    source 116
    target 112
  ]
  edge
  [
    source 117
    target 112
  ]
  edge
  [
    source 118
    target 112
  ]
  edge
  [
    source 113
    target 113
  ]
  edge
  [
    source 114
    target 113
  ]
  edge
  [
    source 115
    target 113
  ]
  edge
  [
    source 116
    target 113
  ]
  edge
  [
    source 117
    target 113
  ]
  edge
  [
    source 118
    target 113
  ]
  edge
  [
    source 114
    target 114
  ]
  edge
  [
    source 115
    target 114
  ]
  edge
  [
    source 116
    target 114
  ]
  edge
  [
    source 117
    target 114
  ]
  edge
  [
    source 118
    target 114
  ]
  edge
  [
    source 115
    target 115
  ]
  edge
  [
    source 116
    target 115
  ]
  edge
  [
    source 117
    target 115
  ]
  edge
  [
    source 118
    target 115
  ]
  edge
  [
    source 116
    target 116
  ]
  edge
  [
    source 117
    target 116
  ]
  edge
  [
    source 118
    target 116
  ]
  edge
  [
    source 117
    target 117
  ]
  edge
  [
    source 118
    target 117
  ]
  edge
  [
    source 118
    target 118
  ]
]
