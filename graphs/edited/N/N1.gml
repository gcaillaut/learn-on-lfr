Creator "igraph version 1.2.2 Wed Oct 24 14:57:36 2018"
Version 1
graph
[
  directed 0
  node
  [
    id 0
    name "1"
    community 2
  ]
  node
  [
    id 1
    name "2"
    community 1
  ]
  node
  [
    id 2
    name "3"
    community 2
  ]
  node
  [
    id 3
    name "4"
    community 2
  ]
  node
  [
    id 4
    name "5"
    community 2
  ]
  node
  [
    id 5
    name "6"
    community 2
  ]
  node
  [
    id 6
    name "7"
    community 1
  ]
  node
  [
    id 7
    name "8"
    community 1
  ]
  node
  [
    id 8
    name "9"
    community 1
  ]
  node
  [
    id 9
    name "10"
    community 1
  ]
  node
  [
    id 10
    name "11"
    community 1
  ]
  node
  [
    id 11
    name "12"
    community 2
  ]
  node
  [
    id 12
    name "13"
    community 2
  ]
  node
  [
    id 13
    name "14"
    community 1
  ]
  node
  [
    id 14
    name "15"
    community 2
  ]
  node
  [
    id 15
    name "16"
    community 2
  ]
  node
  [
    id 16
    name "17"
    community 2
  ]
  node
  [
    id 17
    name "18"
    community 1
  ]
  node
  [
    id 18
    name "19"
    community 1
  ]
  node
  [
    id 19
    name "20"
    community 1
  ]
  node
  [
    id 20
    name "21"
    community 2
  ]
  node
  [
    id 21
    name "22"
    community 2
  ]
  node
  [
    id 22
    name "23"
    community 2
  ]
  node
  [
    id 23
    name "24"
    community 2
  ]
  node
  [
    id 24
    name "25"
    community 1
  ]
  node
  [
    id 25
    name "26"
    community 1
  ]
  node
  [
    id 26
    name "27"
    community 1
  ]
  node
  [
    id 27
    name "28"
    community 2
  ]
  node
  [
    id 28
    name "29"
    community 1
  ]
  node
  [
    id 29
    name "30"
    community 1
  ]
  node
  [
    id 30
    name "31"
    community 1
  ]
  node
  [
    id 31
    name "32"
    community 2
  ]
  node
  [
    id 32
    name "33"
    community 1
  ]
  node
  [
    id 33
    name "34"
    community 2
  ]
  node
  [
    id 34
    name "35"
    community 1
  ]
  node
  [
    id 35
    name "36"
    community 1
  ]
  node
  [
    id 36
    name "37"
    community 1
  ]
  node
  [
    id 37
    name "38"
    community 1
  ]
  node
  [
    id 38
    name "39"
    community 2
  ]
  node
  [
    id 39
    name "40"
    community 1
  ]
  node
  [
    id 40
    name "41"
    community 2
  ]
  node
  [
    id 41
    name "42"
    community 2
  ]
  node
  [
    id 42
    name "43"
    community 2
  ]
  node
  [
    id 43
    name "44"
    community 1
  ]
  node
  [
    id 44
    name "45"
    community 2
  ]
  node
  [
    id 45
    name "46"
    community 1
  ]
  node
  [
    id 46
    name "47"
    community 1
  ]
  node
  [
    id 47
    name "48"
    community 1
  ]
  node
  [
    id 48
    name "49"
    community 1
  ]
  node
  [
    id 49
    name "50"
    community 1
  ]
  edge
  [
    source 16
    target 0
  ]
  edge
  [
    source 20
    target 0
  ]
  edge
  [
    source 21
    target 0
  ]
  edge
  [
    source 27
    target 0
  ]
  edge
  [
    source 28
    target 0
  ]
  edge
  [
    source 33
    target 0
  ]
  edge
  [
    source 35
    target 0
  ]
  edge
  [
    source 40
    target 0
  ]
  edge
  [
    source 41
    target 0
  ]
  edge
  [
    source 42
    target 0
  ]
  edge
  [
    source 44
    target 0
  ]
  edge
  [
    source 45
    target 0
  ]
  edge
  [
    source 47
    target 0
  ]
  edge
  [
    source 3
    target 1
  ]
  edge
  [
    source 6
    target 1
  ]
  edge
  [
    source 10
    target 1
  ]
  edge
  [
    source 25
    target 1
  ]
  edge
  [
    source 29
    target 1
  ]
  edge
  [
    source 35
    target 1
  ]
  edge
  [
    source 41
    target 1
  ]
  edge
  [
    source 44
    target 1
  ]
  edge
  [
    source 45
    target 1
  ]
  edge
  [
    source 46
    target 1
  ]
  edge
  [
    source 47
    target 1
  ]
  edge
  [
    source 48
    target 1
  ]
  edge
  [
    source 49
    target 1
  ]
  edge
  [
    source 5
    target 2
  ]
  edge
  [
    source 13
    target 2
  ]
  edge
  [
    source 20
    target 2
  ]
  edge
  [
    source 22
    target 2
  ]
  edge
  [
    source 27
    target 2
  ]
  edge
  [
    source 33
    target 2
  ]
  edge
  [
    source 35
    target 2
  ]
  edge
  [
    source 40
    target 2
  ]
  edge
  [
    source 41
    target 2
  ]
  edge
  [
    source 42
    target 2
  ]
  edge
  [
    source 44
    target 2
  ]
  edge
  [
    source 45
    target 2
  ]
  edge
  [
    source 47
    target 2
  ]
  edge
  [
    source 3
    target 1
  ]
  edge
  [
    source 21
    target 3
  ]
  edge
  [
    source 25
    target 3
  ]
  edge
  [
    source 27
    target 3
  ]
  edge
  [
    source 31
    target 3
  ]
  edge
  [
    source 33
    target 3
  ]
  edge
  [
    source 36
    target 3
  ]
  edge
  [
    source 38
    target 3
  ]
  edge
  [
    source 40
    target 3
  ]
  edge
  [
    source 41
    target 3
  ]
  edge
  [
    source 42
    target 3
  ]
  edge
  [
    source 44
    target 3
  ]
  edge
  [
    source 45
    target 3
  ]
  edge
  [
    source 13
    target 4
  ]
  edge
  [
    source 15
    target 4
  ]
  edge
  [
    source 16
    target 4
  ]
  edge
  [
    source 20
    target 4
  ]
  edge
  [
    source 23
    target 4
  ]
  edge
  [
    source 27
    target 4
  ]
  edge
  [
    source 32
    target 4
  ]
  edge
  [
    source 34
    target 4
  ]
  edge
  [
    source 37
    target 4
  ]
  edge
  [
    source 38
    target 4
  ]
  edge
  [
    source 40
    target 4
  ]
  edge
  [
    source 41
    target 4
  ]
  edge
  [
    source 44
    target 4
  ]
  edge
  [
    source 5
    target 2
  ]
  edge
  [
    source 6
    target 5
  ]
  edge
  [
    source 7
    target 5
  ]
  edge
  [
    source 14
    target 5
  ]
  edge
  [
    source 15
    target 5
  ]
  edge
  [
    source 21
    target 5
  ]
  edge
  [
    source 23
    target 5
  ]
  edge
  [
    source 31
    target 5
  ]
  edge
  [
    source 33
    target 5
  ]
  edge
  [
    source 34
    target 5
  ]
  edge
  [
    source 38
    target 5
  ]
  edge
  [
    source 42
    target 5
  ]
  edge
  [
    source 44
    target 5
  ]
  edge
  [
    source 48
    target 5
  ]
  edge
  [
    source 6
    target 1
  ]
  edge
  [
    source 6
    target 5
  ]
  edge
  [
    source 17
    target 6
  ]
  edge
  [
    source 18
    target 6
  ]
  edge
  [
    source 19
    target 6
  ]
  edge
  [
    source 20
    target 6
  ]
  edge
  [
    source 24
    target 6
  ]
  edge
  [
    source 28
    target 6
  ]
  edge
  [
    source 39
    target 6
  ]
  edge
  [
    source 43
    target 6
  ]
  edge
  [
    source 45
    target 6
  ]
  edge
  [
    source 46
    target 6
  ]
  edge
  [
    source 48
    target 6
  ]
  edge
  [
    source 49
    target 6
  ]
  edge
  [
    source 7
    target 5
  ]
  edge
  [
    source 9
    target 7
  ]
  edge
  [
    source 21
    target 7
  ]
  edge
  [
    source 26
    target 7
  ]
  edge
  [
    source 29
    target 7
  ]
  edge
  [
    source 31
    target 7
  ]
  edge
  [
    source 37
    target 7
  ]
  edge
  [
    source 41
    target 7
  ]
  edge
  [
    source 43
    target 7
  ]
  edge
  [
    source 45
    target 7
  ]
  edge
  [
    source 46
    target 7
  ]
  edge
  [
    source 47
    target 7
  ]
  edge
  [
    source 48
    target 7
  ]
  edge
  [
    source 49
    target 7
  ]
  edge
  [
    source 17
    target 8
  ]
  edge
  [
    source 19
    target 8
  ]
  edge
  [
    source 24
    target 8
  ]
  edge
  [
    source 25
    target 8
  ]
  edge
  [
    source 26
    target 8
  ]
  edge
  [
    source 32
    target 8
  ]
  edge
  [
    source 39
    target 8
  ]
  edge
  [
    source 43
    target 8
  ]
  edge
  [
    source 46
    target 8
  ]
  edge
  [
    source 47
    target 8
  ]
  edge
  [
    source 48
    target 8
  ]
  edge
  [
    source 49
    target 8
  ]
  edge
  [
    source 9
    target 7
  ]
  edge
  [
    source 10
    target 9
  ]
  edge
  [
    source 18
    target 9
  ]
  edge
  [
    source 20
    target 9
  ]
  edge
  [
    source 25
    target 9
  ]
  edge
  [
    source 30
    target 9
  ]
  edge
  [
    source 31
    target 9
  ]
  edge
  [
    source 32
    target 9
  ]
  edge
  [
    source 34
    target 9
  ]
  edge
  [
    source 35
    target 9
  ]
  edge
  [
    source 46
    target 9
  ]
  edge
  [
    source 47
    target 9
  ]
  edge
  [
    source 48
    target 9
  ]
  edge
  [
    source 49
    target 9
  ]
  edge
  [
    source 10
    target 1
  ]
  edge
  [
    source 10
    target 9
  ]
  edge
  [
    source 25
    target 10
  ]
  edge
  [
    source 26
    target 10
  ]
  edge
  [
    source 28
    target 10
  ]
  edge
  [
    source 33
    target 10
  ]
  edge
  [
    source 36
    target 10
  ]
  edge
  [
    source 42
    target 10
  ]
  edge
  [
    source 43
    target 10
  ]
  edge
  [
    source 44
    target 10
  ]
  edge
  [
    source 45
    target 10
  ]
  edge
  [
    source 47
    target 10
  ]
  edge
  [
    source 48
    target 10
  ]
  edge
  [
    source 49
    target 10
  ]
  edge
  [
    source 14
    target 11
  ]
  edge
  [
    source 15
    target 11
  ]
  edge
  [
    source 19
    target 11
  ]
  edge
  [
    source 20
    target 11
  ]
  edge
  [
    source 23
    target 11
  ]
  edge
  [
    source 26
    target 11
  ]
  edge
  [
    source 31
    target 11
  ]
  edge
  [
    source 33
    target 11
  ]
  edge
  [
    source 38
    target 11
  ]
  edge
  [
    source 40
    target 11
  ]
  edge
  [
    source 42
    target 11
  ]
  edge
  [
    source 44
    target 11
  ]
  edge
  [
    source 46
    target 11
  ]
  edge
  [
    source 49
    target 11
  ]
  edge
  [
    source 14
    target 12
  ]
  edge
  [
    source 16
    target 12
  ]
  edge
  [
    source 19
    target 12
  ]
  edge
  [
    source 20
    target 12
  ]
  edge
  [
    source 21
    target 12
  ]
  edge
  [
    source 22
    target 12
  ]
  edge
  [
    source 31
    target 12
  ]
  edge
  [
    source 33
    target 12
  ]
  edge
  [
    source 34
    target 12
  ]
  edge
  [
    source 36
    target 12
  ]
  edge
  [
    source 41
    target 12
  ]
  edge
  [
    source 42
    target 12
  ]
  edge
  [
    source 44
    target 12
  ]
  edge
  [
    source 49
    target 12
  ]
  edge
  [
    source 13
    target 2
  ]
  edge
  [
    source 13
    target 4
  ]
  edge
  [
    source 17
    target 13
  ]
  edge
  [
    source 22
    target 13
  ]
  edge
  [
    source 24
    target 13
  ]
  edge
  [
    source 29
    target 13
  ]
  edge
  [
    source 34
    target 13
  ]
  edge
  [
    source 36
    target 13
  ]
  edge
  [
    source 37
    target 13
  ]
  edge
  [
    source 43
    target 13
  ]
  edge
  [
    source 45
    target 13
  ]
  edge
  [
    source 47
    target 13
  ]
  edge
  [
    source 48
    target 13
  ]
  edge
  [
    source 49
    target 13
  ]
  edge
  [
    source 14
    target 5
  ]
  edge
  [
    source 14
    target 11
  ]
  edge
  [
    source 14
    target 12
  ]
  edge
  [
    source 22
    target 14
  ]
  edge
  [
    source 26
    target 14
  ]
  edge
  [
    source 27
    target 14
  ]
  edge
  [
    source 29
    target 14
  ]
  edge
  [
    source 30
    target 14
  ]
  edge
  [
    source 31
    target 14
  ]
  edge
  [
    source 38
    target 14
  ]
  edge
  [
    source 41
    target 14
  ]
  edge
  [
    source 42
    target 14
  ]
  edge
  [
    source 44
    target 14
  ]
  edge
  [
    source 45
    target 14
  ]
  edge
  [
    source 47
    target 14
  ]
  edge
  [
    source 15
    target 4
  ]
  edge
  [
    source 15
    target 5
  ]
  edge
  [
    source 15
    target 11
  ]
  edge
  [
    source 16
    target 15
  ]
  edge
  [
    source 22
    target 15
  ]
  edge
  [
    source 27
    target 15
  ]
  edge
  [
    source 32
    target 15
  ]
  edge
  [
    source 37
    target 15
  ]
  edge
  [
    source 39
    target 15
  ]
  edge
  [
    source 40
    target 15
  ]
  edge
  [
    source 41
    target 15
  ]
  edge
  [
    source 42
    target 15
  ]
  edge
  [
    source 43
    target 15
  ]
  edge
  [
    source 44
    target 15
  ]
  edge
  [
    source 46
    target 15
  ]
  edge
  [
    source 16
    target 0
  ]
  edge
  [
    source 16
    target 4
  ]
  edge
  [
    source 16
    target 12
  ]
  edge
  [
    source 16
    target 15
  ]
  edge
  [
    source 22
    target 16
  ]
  edge
  [
    source 23
    target 16
  ]
  edge
  [
    source 33
    target 16
  ]
  edge
  [
    source 34
    target 16
  ]
  edge
  [
    source 38
    target 16
  ]
  edge
  [
    source 40
    target 16
  ]
  edge
  [
    source 43
    target 16
  ]
  edge
  [
    source 44
    target 16
  ]
  edge
  [
    source 45
    target 16
  ]
  edge
  [
    source 46
    target 16
  ]
  edge
  [
    source 47
    target 16
  ]
  edge
  [
    source 17
    target 6
  ]
  edge
  [
    source 17
    target 8
  ]
  edge
  [
    source 17
    target 13
  ]
  edge
  [
    source 18
    target 17
  ]
  edge
  [
    source 29
    target 17
  ]
  edge
  [
    source 30
    target 17
  ]
  edge
  [
    source 36
    target 17
  ]
  edge
  [
    source 39
    target 17
  ]
  edge
  [
    source 40
    target 17
  ]
  edge
  [
    source 43
    target 17
  ]
  edge
  [
    source 45
    target 17
  ]
  edge
  [
    source 46
    target 17
  ]
  edge
  [
    source 47
    target 17
  ]
  edge
  [
    source 48
    target 17
  ]
  edge
  [
    source 49
    target 17
  ]
  edge
  [
    source 18
    target 6
  ]
  edge
  [
    source 18
    target 9
  ]
  edge
  [
    source 18
    target 17
  ]
  edge
  [
    source 23
    target 18
  ]
  edge
  [
    source 28
    target 18
  ]
  edge
  [
    source 29
    target 18
  ]
  edge
  [
    source 32
    target 18
  ]
  edge
  [
    source 33
    target 18
  ]
  edge
  [
    source 34
    target 18
  ]
  edge
  [
    source 39
    target 18
  ]
  edge
  [
    source 41
    target 18
  ]
  edge
  [
    source 45
    target 18
  ]
  edge
  [
    source 46
    target 18
  ]
  edge
  [
    source 47
    target 18
  ]
  edge
  [
    source 48
    target 18
  ]
  edge
  [
    source 49
    target 18
  ]
  edge
  [
    source 19
    target 6
  ]
  edge
  [
    source 19
    target 8
  ]
  edge
  [
    source 19
    target 11
  ]
  edge
  [
    source 19
    target 12
  ]
  edge
  [
    source 23
    target 19
  ]
  edge
  [
    source 28
    target 19
  ]
  edge
  [
    source 36
    target 19
  ]
  edge
  [
    source 37
    target 19
  ]
  edge
  [
    source 38
    target 19
  ]
  edge
  [
    source 39
    target 19
  ]
  edge
  [
    source 40
    target 19
  ]
  edge
  [
    source 43
    target 19
  ]
  edge
  [
    source 46
    target 19
  ]
  edge
  [
    source 47
    target 19
  ]
  edge
  [
    source 48
    target 19
  ]
  edge
  [
    source 49
    target 19
  ]
  edge
  [
    source 20
    target 0
  ]
  edge
  [
    source 20
    target 2
  ]
  edge
  [
    source 20
    target 4
  ]
  edge
  [
    source 20
    target 6
  ]
  edge
  [
    source 20
    target 9
  ]
  edge
  [
    source 20
    target 11
  ]
  edge
  [
    source 20
    target 12
  ]
  edge
  [
    source 27
    target 20
  ]
  edge
  [
    source 29
    target 20
  ]
  edge
  [
    source 31
    target 20
  ]
  edge
  [
    source 37
    target 20
  ]
  edge
  [
    source 38
    target 20
  ]
  edge
  [
    source 41
    target 20
  ]
  edge
  [
    source 42
    target 20
  ]
  edge
  [
    source 43
    target 20
  ]
  edge
  [
    source 44
    target 20
  ]
  edge
  [
    source 21
    target 0
  ]
  edge
  [
    source 21
    target 3
  ]
  edge
  [
    source 21
    target 5
  ]
  edge
  [
    source 21
    target 7
  ]
  edge
  [
    source 21
    target 12
  ]
  edge
  [
    source 22
    target 21
  ]
  edge
  [
    source 23
    target 21
  ]
  edge
  [
    source 26
    target 21
  ]
  edge
  [
    source 28
    target 21
  ]
  edge
  [
    source 31
    target 21
  ]
  edge
  [
    source 36
    target 21
  ]
  edge
  [
    source 40
    target 21
  ]
  edge
  [
    source 41
    target 21
  ]
  edge
  [
    source 42
    target 21
  ]
  edge
  [
    source 44
    target 21
  ]
  edge
  [
    source 45
    target 21
  ]
  edge
  [
    source 48
    target 21
  ]
  edge
  [
    source 22
    target 2
  ]
  edge
  [
    source 22
    target 12
  ]
  edge
  [
    source 22
    target 13
  ]
  edge
  [
    source 22
    target 14
  ]
  edge
  [
    source 22
    target 15
  ]
  edge
  [
    source 22
    target 16
  ]
  edge
  [
    source 22
    target 21
  ]
  edge
  [
    source 23
    target 22
  ]
  edge
  [
    source 30
    target 22
  ]
  edge
  [
    source 32
    target 22
  ]
  edge
  [
    source 37
    target 22
  ]
  edge
  [
    source 38
    target 22
  ]
  edge
  [
    source 41
    target 22
  ]
  edge
  [
    source 42
    target 22
  ]
  edge
  [
    source 44
    target 22
  ]
  edge
  [
    source 46
    target 22
  ]
  edge
  [
    source 47
    target 22
  ]
  edge
  [
    source 23
    target 4
  ]
  edge
  [
    source 23
    target 5
  ]
  edge
  [
    source 23
    target 11
  ]
  edge
  [
    source 23
    target 16
  ]
  edge
  [
    source 23
    target 18
  ]
  edge
  [
    source 23
    target 19
  ]
  edge
  [
    source 23
    target 21
  ]
  edge
  [
    source 23
    target 22
  ]
  edge
  [
    source 28
    target 23
  ]
  edge
  [
    source 33
    target 23
  ]
  edge
  [
    source 38
    target 23
  ]
  edge
  [
    source 39
    target 23
  ]
  edge
  [
    source 40
    target 23
  ]
  edge
  [
    source 41
    target 23
  ]
  edge
  [
    source 42
    target 23
  ]
  edge
  [
    source 44
    target 23
  ]
  edge
  [
    source 45
    target 23
  ]
  edge
  [
    source 24
    target 6
  ]
  edge
  [
    source 24
    target 8
  ]
  edge
  [
    source 24
    target 13
  ]
  edge
  [
    source 25
    target 24
  ]
  edge
  [
    source 34
    target 24
  ]
  edge
  [
    source 35
    target 24
  ]
  edge
  [
    source 37
    target 24
  ]
  edge
  [
    source 41
    target 24
  ]
  edge
  [
    source 42
    target 24
  ]
  edge
  [
    source 43
    target 24
  ]
  edge
  [
    source 44
    target 24
  ]
  edge
  [
    source 45
    target 24
  ]
  edge
  [
    source 46
    target 24
  ]
  edge
  [
    source 47
    target 24
  ]
  edge
  [
    source 48
    target 24
  ]
  edge
  [
    source 49
    target 24
  ]
  edge
  [
    source 25
    target 1
  ]
  edge
  [
    source 25
    target 3
  ]
  edge
  [
    source 25
    target 8
  ]
  edge
  [
    source 25
    target 9
  ]
  edge
  [
    source 25
    target 10
  ]
  edge
  [
    source 25
    target 24
  ]
  edge
  [
    source 27
    target 25
  ]
  edge
  [
    source 29
    target 25
  ]
  edge
  [
    source 34
    target 25
  ]
  edge
  [
    source 35
    target 25
  ]
  edge
  [
    source 37
    target 25
  ]
  edge
  [
    source 39
    target 25
  ]
  edge
  [
    source 44
    target 25
  ]
  edge
  [
    source 45
    target 25
  ]
  edge
  [
    source 46
    target 25
  ]
  edge
  [
    source 47
    target 25
  ]
  edge
  [
    source 48
    target 25
  ]
  edge
  [
    source 49
    target 25
  ]
  edge
  [
    source 26
    target 7
  ]
  edge
  [
    source 26
    target 8
  ]
  edge
  [
    source 26
    target 10
  ]
  edge
  [
    source 26
    target 11
  ]
  edge
  [
    source 26
    target 14
  ]
  edge
  [
    source 26
    target 21
  ]
  edge
  [
    source 28
    target 26
  ]
  edge
  [
    source 29
    target 26
  ]
  edge
  [
    source 32
    target 26
  ]
  edge
  [
    source 37
    target 26
  ]
  edge
  [
    source 38
    target 26
  ]
  edge
  [
    source 40
    target 26
  ]
  edge
  [
    source 45
    target 26
  ]
  edge
  [
    source 46
    target 26
  ]
  edge
  [
    source 47
    target 26
  ]
  edge
  [
    source 48
    target 26
  ]
  edge
  [
    source 49
    target 26
  ]
  edge
  [
    source 27
    target 0
  ]
  edge
  [
    source 27
    target 2
  ]
  edge
  [
    source 27
    target 3
  ]
  edge
  [
    source 27
    target 4
  ]
  edge
  [
    source 27
    target 14
  ]
  edge
  [
    source 27
    target 15
  ]
  edge
  [
    source 27
    target 20
  ]
  edge
  [
    source 27
    target 25
  ]
  edge
  [
    source 29
    target 27
  ]
  edge
  [
    source 32
    target 27
  ]
  edge
  [
    source 33
    target 27
  ]
  edge
  [
    source 38
    target 27
  ]
  edge
  [
    source 40
    target 27
  ]
  edge
  [
    source 41
    target 27
  ]
  edge
  [
    source 42
    target 27
  ]
  edge
  [
    source 44
    target 27
  ]
  edge
  [
    source 45
    target 27
  ]
  edge
  [
    source 49
    target 27
  ]
  edge
  [
    source 28
    target 0
  ]
  edge
  [
    source 28
    target 6
  ]
  edge
  [
    source 28
    target 10
  ]
  edge
  [
    source 28
    target 18
  ]
  edge
  [
    source 28
    target 19
  ]
  edge
  [
    source 28
    target 21
  ]
  edge
  [
    source 28
    target 23
  ]
  edge
  [
    source 28
    target 26
  ]
  edge
  [
    source 33
    target 28
  ]
  edge
  [
    source 34
    target 28
  ]
  edge
  [
    source 35
    target 28
  ]
  edge
  [
    source 37
    target 28
  ]
  edge
  [
    source 38
    target 28
  ]
  edge
  [
    source 39
    target 28
  ]
  edge
  [
    source 44
    target 28
  ]
  edge
  [
    source 46
    target 28
  ]
  edge
  [
    source 47
    target 28
  ]
  edge
  [
    source 48
    target 28
  ]
  edge
  [
    source 49
    target 28
  ]
  edge
  [
    source 29
    target 1
  ]
  edge
  [
    source 29
    target 7
  ]
  edge
  [
    source 29
    target 13
  ]
  edge
  [
    source 29
    target 14
  ]
  edge
  [
    source 29
    target 17
  ]
  edge
  [
    source 29
    target 18
  ]
  edge
  [
    source 29
    target 20
  ]
  edge
  [
    source 29
    target 25
  ]
  edge
  [
    source 29
    target 26
  ]
  edge
  [
    source 29
    target 27
  ]
  edge
  [
    source 33
    target 29
  ]
  edge
  [
    source 35
    target 29
  ]
  edge
  [
    source 36
    target 29
  ]
  edge
  [
    source 39
    target 29
  ]
  edge
  [
    source 40
    target 29
  ]
  edge
  [
    source 45
    target 29
  ]
  edge
  [
    source 46
    target 29
  ]
  edge
  [
    source 48
    target 29
  ]
  edge
  [
    source 49
    target 29
  ]
  edge
  [
    source 30
    target 9
  ]
  edge
  [
    source 30
    target 14
  ]
  edge
  [
    source 30
    target 17
  ]
  edge
  [
    source 30
    target 22
  ]
  edge
  [
    source 32
    target 30
  ]
  edge
  [
    source 35
    target 30
  ]
  edge
  [
    source 36
    target 30
  ]
  edge
  [
    source 37
    target 30
  ]
  edge
  [
    source 39
    target 30
  ]
  edge
  [
    source 42
    target 30
  ]
  edge
  [
    source 43
    target 30
  ]
  edge
  [
    source 44
    target 30
  ]
  edge
  [
    source 45
    target 30
  ]
  edge
  [
    source 46
    target 30
  ]
  edge
  [
    source 47
    target 30
  ]
  edge
  [
    source 48
    target 30
  ]
  edge
  [
    source 49
    target 30
  ]
  edge
  [
    source 31
    target 3
  ]
  edge
  [
    source 31
    target 5
  ]
  edge
  [
    source 31
    target 7
  ]
  edge
  [
    source 31
    target 9
  ]
  edge
  [
    source 31
    target 11
  ]
  edge
  [
    source 31
    target 12
  ]
  edge
  [
    source 31
    target 14
  ]
  edge
  [
    source 31
    target 20
  ]
  edge
  [
    source 31
    target 21
  ]
  edge
  [
    source 33
    target 31
  ]
  edge
  [
    source 36
    target 31
  ]
  edge
  [
    source 38
    target 31
  ]
  edge
  [
    source 40
    target 31
  ]
  edge
  [
    source 41
    target 31
  ]
  edge
  [
    source 42
    target 31
  ]
  edge
  [
    source 43
    target 31
  ]
  edge
  [
    source 44
    target 31
  ]
  edge
  [
    source 46
    target 31
  ]
  edge
  [
    source 48
    target 31
  ]
  edge
  [
    source 32
    target 4
  ]
  edge
  [
    source 32
    target 8
  ]
  edge
  [
    source 32
    target 9
  ]
  edge
  [
    source 32
    target 15
  ]
  edge
  [
    source 32
    target 18
  ]
  edge
  [
    source 32
    target 22
  ]
  edge
  [
    source 32
    target 26
  ]
  edge
  [
    source 32
    target 27
  ]
  edge
  [
    source 32
    target 30
  ]
  edge
  [
    source 34
    target 32
  ]
  edge
  [
    source 35
    target 32
  ]
  edge
  [
    source 38
    target 32
  ]
  edge
  [
    source 42
    target 32
  ]
  edge
  [
    source 43
    target 32
  ]
  edge
  [
    source 45
    target 32
  ]
  edge
  [
    source 46
    target 32
  ]
  edge
  [
    source 47
    target 32
  ]
  edge
  [
    source 48
    target 32
  ]
  edge
  [
    source 49
    target 32
  ]
  edge
  [
    source 33
    target 0
  ]
  edge
  [
    source 33
    target 2
  ]
  edge
  [
    source 33
    target 3
  ]
  edge
  [
    source 33
    target 5
  ]
  edge
  [
    source 33
    target 10
  ]
  edge
  [
    source 33
    target 11
  ]
  edge
  [
    source 33
    target 12
  ]
  edge
  [
    source 33
    target 16
  ]
  edge
  [
    source 33
    target 18
  ]
  edge
  [
    source 33
    target 23
  ]
  edge
  [
    source 33
    target 27
  ]
  edge
  [
    source 33
    target 28
  ]
  edge
  [
    source 33
    target 29
  ]
  edge
  [
    source 33
    target 31
  ]
  edge
  [
    source 37
    target 33
  ]
  edge
  [
    source 38
    target 33
  ]
  edge
  [
    source 40
    target 33
  ]
  edge
  [
    source 42
    target 33
  ]
  edge
  [
    source 44
    target 33
  ]
  edge
  [
    source 48
    target 33
  ]
  edge
  [
    source 34
    target 4
  ]
  edge
  [
    source 34
    target 5
  ]
  edge
  [
    source 34
    target 9
  ]
  edge
  [
    source 34
    target 12
  ]
  edge
  [
    source 34
    target 13
  ]
  edge
  [
    source 34
    target 16
  ]
  edge
  [
    source 34
    target 18
  ]
  edge
  [
    source 34
    target 24
  ]
  edge
  [
    source 34
    target 25
  ]
  edge
  [
    source 34
    target 28
  ]
  edge
  [
    source 34
    target 32
  ]
  edge
  [
    source 36
    target 34
  ]
  edge
  [
    source 38
    target 34
  ]
  edge
  [
    source 39
    target 34
  ]
  edge
  [
    source 43
    target 34
  ]
  edge
  [
    source 44
    target 34
  ]
  edge
  [
    source 45
    target 34
  ]
  edge
  [
    source 46
    target 34
  ]
  edge
  [
    source 47
    target 34
  ]
  edge
  [
    source 48
    target 34
  ]
  edge
  [
    source 49
    target 34
  ]
  edge
  [
    source 35
    target 0
  ]
  edge
  [
    source 35
    target 1
  ]
  edge
  [
    source 35
    target 2
  ]
  edge
  [
    source 35
    target 9
  ]
  edge
  [
    source 35
    target 24
  ]
  edge
  [
    source 35
    target 25
  ]
  edge
  [
    source 35
    target 28
  ]
  edge
  [
    source 35
    target 29
  ]
  edge
  [
    source 35
    target 30
  ]
  edge
  [
    source 35
    target 32
  ]
  edge
  [
    source 36
    target 35
  ]
  edge
  [
    source 40
    target 35
  ]
  edge
  [
    source 43
    target 35
  ]
  edge
  [
    source 45
    target 35
  ]
  edge
  [
    source 46
    target 35
  ]
  edge
  [
    source 47
    target 35
  ]
  edge
  [
    source 48
    target 35
  ]
  edge
  [
    source 49
    target 35
  ]
  edge
  [
    source 36
    target 3
  ]
  edge
  [
    source 36
    target 10
  ]
  edge
  [
    source 36
    target 12
  ]
  edge
  [
    source 36
    target 13
  ]
  edge
  [
    source 36
    target 17
  ]
  edge
  [
    source 36
    target 19
  ]
  edge
  [
    source 36
    target 21
  ]
  edge
  [
    source 36
    target 29
  ]
  edge
  [
    source 36
    target 30
  ]
  edge
  [
    source 36
    target 31
  ]
  edge
  [
    source 36
    target 34
  ]
  edge
  [
    source 36
    target 35
  ]
  edge
  [
    source 37
    target 36
  ]
  edge
  [
    source 38
    target 36
  ]
  edge
  [
    source 39
    target 36
  ]
  edge
  [
    source 42
    target 36
  ]
  edge
  [
    source 43
    target 36
  ]
  edge
  [
    source 45
    target 36
  ]
  edge
  [
    source 47
    target 36
  ]
  edge
  [
    source 48
    target 36
  ]
  edge
  [
    source 49
    target 36
  ]
  edge
  [
    source 37
    target 4
  ]
  edge
  [
    source 37
    target 7
  ]
  edge
  [
    source 37
    target 13
  ]
  edge
  [
    source 37
    target 15
  ]
  edge
  [
    source 37
    target 19
  ]
  edge
  [
    source 37
    target 20
  ]
  edge
  [
    source 37
    target 22
  ]
  edge
  [
    source 37
    target 24
  ]
  edge
  [
    source 37
    target 25
  ]
  edge
  [
    source 37
    target 26
  ]
  edge
  [
    source 37
    target 28
  ]
  edge
  [
    source 37
    target 30
  ]
  edge
  [
    source 37
    target 33
  ]
  edge
  [
    source 37
    target 36
  ]
  edge
  [
    source 43
    target 37
  ]
  edge
  [
    source 44
    target 37
  ]
  edge
  [
    source 45
    target 37
  ]
  edge
  [
    source 46
    target 37
  ]
  edge
  [
    source 47
    target 37
  ]
  edge
  [
    source 48
    target 37
  ]
  edge
  [
    source 49
    target 37
  ]
  edge
  [
    source 38
    target 3
  ]
  edge
  [
    source 38
    target 4
  ]
  edge
  [
    source 38
    target 5
  ]
  edge
  [
    source 38
    target 11
  ]
  edge
  [
    source 38
    target 14
  ]
  edge
  [
    source 38
    target 16
  ]
  edge
  [
    source 38
    target 19
  ]
  edge
  [
    source 38
    target 20
  ]
  edge
  [
    source 38
    target 22
  ]
  edge
  [
    source 38
    target 23
  ]
  edge
  [
    source 38
    target 26
  ]
  edge
  [
    source 38
    target 27
  ]
  edge
  [
    source 38
    target 28
  ]
  edge
  [
    source 38
    target 31
  ]
  edge
  [
    source 38
    target 32
  ]
  edge
  [
    source 38
    target 33
  ]
  edge
  [
    source 38
    target 34
  ]
  edge
  [
    source 38
    target 36
  ]
  edge
  [
    source 40
    target 38
  ]
  edge
  [
    source 41
    target 38
  ]
  edge
  [
    source 42
    target 38
  ]
  edge
  [
    source 44
    target 38
  ]
  edge
  [
    source 39
    target 6
  ]
  edge
  [
    source 39
    target 8
  ]
  edge
  [
    source 39
    target 15
  ]
  edge
  [
    source 39
    target 17
  ]
  edge
  [
    source 39
    target 18
  ]
  edge
  [
    source 39
    target 19
  ]
  edge
  [
    source 39
    target 23
  ]
  edge
  [
    source 39
    target 25
  ]
  edge
  [
    source 39
    target 28
  ]
  edge
  [
    source 39
    target 29
  ]
  edge
  [
    source 39
    target 30
  ]
  edge
  [
    source 39
    target 34
  ]
  edge
  [
    source 39
    target 36
  ]
  edge
  [
    source 41
    target 39
  ]
  edge
  [
    source 43
    target 39
  ]
  edge
  [
    source 45
    target 39
  ]
  edge
  [
    source 46
    target 39
  ]
  edge
  [
    source 47
    target 39
  ]
  edge
  [
    source 48
    target 39
  ]
  edge
  [
    source 49
    target 39
  ]
  edge
  [
    source 40
    target 0
  ]
  edge
  [
    source 40
    target 2
  ]
  edge
  [
    source 40
    target 3
  ]
  edge
  [
    source 40
    target 4
  ]
  edge
  [
    source 40
    target 11
  ]
  edge
  [
    source 40
    target 15
  ]
  edge
  [
    source 40
    target 16
  ]
  edge
  [
    source 40
    target 17
  ]
  edge
  [
    source 40
    target 19
  ]
  edge
  [
    source 40
    target 21
  ]
  edge
  [
    source 40
    target 23
  ]
  edge
  [
    source 40
    target 26
  ]
  edge
  [
    source 40
    target 27
  ]
  edge
  [
    source 40
    target 29
  ]
  edge
  [
    source 40
    target 31
  ]
  edge
  [
    source 40
    target 33
  ]
  edge
  [
    source 40
    target 35
  ]
  edge
  [
    source 40
    target 38
  ]
  edge
  [
    source 41
    target 40
  ]
  edge
  [
    source 42
    target 40
  ]
  edge
  [
    source 44
    target 40
  ]
  edge
  [
    source 46
    target 40
  ]
  edge
  [
    source 48
    target 40
  ]
  edge
  [
    source 49
    target 40
  ]
  edge
  [
    source 41
    target 0
  ]
  edge
  [
    source 41
    target 1
  ]
  edge
  [
    source 41
    target 2
  ]
  edge
  [
    source 41
    target 3
  ]
  edge
  [
    source 41
    target 4
  ]
  edge
  [
    source 41
    target 7
  ]
  edge
  [
    source 41
    target 12
  ]
  edge
  [
    source 41
    target 14
  ]
  edge
  [
    source 41
    target 15
  ]
  edge
  [
    source 41
    target 18
  ]
  edge
  [
    source 41
    target 20
  ]
  edge
  [
    source 41
    target 21
  ]
  edge
  [
    source 41
    target 22
  ]
  edge
  [
    source 41
    target 23
  ]
  edge
  [
    source 41
    target 24
  ]
  edge
  [
    source 41
    target 27
  ]
  edge
  [
    source 41
    target 31
  ]
  edge
  [
    source 41
    target 38
  ]
  edge
  [
    source 41
    target 39
  ]
  edge
  [
    source 41
    target 40
  ]
  edge
  [
    source 42
    target 41
  ]
  edge
  [
    source 43
    target 41
  ]
  edge
  [
    source 44
    target 41
  ]
  edge
  [
    source 46
    target 41
  ]
  edge
  [
    source 47
    target 41
  ]
  edge
  [
    source 42
    target 0
  ]
  edge
  [
    source 42
    target 2
  ]
  edge
  [
    source 42
    target 3
  ]
  edge
  [
    source 42
    target 5
  ]
  edge
  [
    source 42
    target 10
  ]
  edge
  [
    source 42
    target 11
  ]
  edge
  [
    source 42
    target 12
  ]
  edge
  [
    source 42
    target 14
  ]
  edge
  [
    source 42
    target 15
  ]
  edge
  [
    source 42
    target 20
  ]
  edge
  [
    source 42
    target 21
  ]
  edge
  [
    source 42
    target 22
  ]
  edge
  [
    source 42
    target 23
  ]
  edge
  [
    source 42
    target 24
  ]
  edge
  [
    source 42
    target 27
  ]
  edge
  [
    source 42
    target 30
  ]
  edge
  [
    source 42
    target 31
  ]
  edge
  [
    source 42
    target 32
  ]
  edge
  [
    source 42
    target 33
  ]
  edge
  [
    source 42
    target 36
  ]
  edge
  [
    source 42
    target 38
  ]
  edge
  [
    source 42
    target 40
  ]
  edge
  [
    source 42
    target 41
  ]
  edge
  [
    source 44
    target 42
  ]
  edge
  [
    source 48
    target 42
  ]
  edge
  [
    source 49
    target 42
  ]
  edge
  [
    source 43
    target 6
  ]
  edge
  [
    source 43
    target 7
  ]
  edge
  [
    source 43
    target 8
  ]
  edge
  [
    source 43
    target 10
  ]
  edge
  [
    source 43
    target 13
  ]
  edge
  [
    source 43
    target 15
  ]
  edge
  [
    source 43
    target 16
  ]
  edge
  [
    source 43
    target 17
  ]
  edge
  [
    source 43
    target 19
  ]
  edge
  [
    source 43
    target 20
  ]
  edge
  [
    source 43
    target 24
  ]
  edge
  [
    source 43
    target 30
  ]
  edge
  [
    source 43
    target 31
  ]
  edge
  [
    source 43
    target 32
  ]
  edge
  [
    source 43
    target 34
  ]
  edge
  [
    source 43
    target 35
  ]
  edge
  [
    source 43
    target 36
  ]
  edge
  [
    source 43
    target 37
  ]
  edge
  [
    source 43
    target 39
  ]
  edge
  [
    source 43
    target 41
  ]
  edge
  [
    source 45
    target 43
  ]
  edge
  [
    source 46
    target 43
  ]
  edge
  [
    source 47
    target 43
  ]
  edge
  [
    source 48
    target 43
  ]
  edge
  [
    source 49
    target 43
  ]
  edge
  [
    source 44
    target 0
  ]
  edge
  [
    source 44
    target 1
  ]
  edge
  [
    source 44
    target 2
  ]
  edge
  [
    source 44
    target 3
  ]
  edge
  [
    source 44
    target 4
  ]
  edge
  [
    source 44
    target 5
  ]
  edge
  [
    source 44
    target 10
  ]
  edge
  [
    source 44
    target 11
  ]
  edge
  [
    source 44
    target 12
  ]
  edge
  [
    source 44
    target 14
  ]
  edge
  [
    source 44
    target 15
  ]
  edge
  [
    source 44
    target 16
  ]
  edge
  [
    source 44
    target 20
  ]
  edge
  [
    source 44
    target 21
  ]
  edge
  [
    source 44
    target 22
  ]
  edge
  [
    source 44
    target 23
  ]
  edge
  [
    source 44
    target 24
  ]
  edge
  [
    source 44
    target 25
  ]
  edge
  [
    source 44
    target 27
  ]
  edge
  [
    source 44
    target 28
  ]
  edge
  [
    source 44
    target 30
  ]
  edge
  [
    source 44
    target 31
  ]
  edge
  [
    source 44
    target 33
  ]
  edge
  [
    source 44
    target 34
  ]
  edge
  [
    source 44
    target 37
  ]
  edge
  [
    source 44
    target 38
  ]
  edge
  [
    source 44
    target 40
  ]
  edge
  [
    source 44
    target 41
  ]
  edge
  [
    source 44
    target 42
  ]
  edge
  [
    source 48
    target 44
  ]
  edge
  [
    source 49
    target 44
  ]
  edge
  [
    source 45
    target 0
  ]
  edge
  [
    source 45
    target 1
  ]
  edge
  [
    source 45
    target 2
  ]
  edge
  [
    source 45
    target 3
  ]
  edge
  [
    source 45
    target 6
  ]
  edge
  [
    source 45
    target 7
  ]
  edge
  [
    source 45
    target 10
  ]
  edge
  [
    source 45
    target 13
  ]
  edge
  [
    source 45
    target 14
  ]
  edge
  [
    source 45
    target 16
  ]
  edge
  [
    source 45
    target 17
  ]
  edge
  [
    source 45
    target 18
  ]
  edge
  [
    source 45
    target 21
  ]
  edge
  [
    source 45
    target 23
  ]
  edge
  [
    source 45
    target 24
  ]
  edge
  [
    source 45
    target 25
  ]
  edge
  [
    source 45
    target 26
  ]
  edge
  [
    source 45
    target 27
  ]
  edge
  [
    source 45
    target 29
  ]
  edge
  [
    source 45
    target 30
  ]
  edge
  [
    source 45
    target 32
  ]
  edge
  [
    source 45
    target 34
  ]
  edge
  [
    source 45
    target 35
  ]
  edge
  [
    source 45
    target 36
  ]
  edge
  [
    source 45
    target 37
  ]
  edge
  [
    source 45
    target 39
  ]
  edge
  [
    source 45
    target 43
  ]
  edge
  [
    source 46
    target 45
  ]
  edge
  [
    source 47
    target 45
  ]
  edge
  [
    source 48
    target 45
  ]
  edge
  [
    source 49
    target 45
  ]
  edge
  [
    source 46
    target 1
  ]
  edge
  [
    source 46
    target 6
  ]
  edge
  [
    source 46
    target 7
  ]
  edge
  [
    source 46
    target 8
  ]
  edge
  [
    source 46
    target 9
  ]
  edge
  [
    source 46
    target 11
  ]
  edge
  [
    source 46
    target 15
  ]
  edge
  [
    source 46
    target 16
  ]
  edge
  [
    source 46
    target 17
  ]
  edge
  [
    source 46
    target 18
  ]
  edge
  [
    source 46
    target 19
  ]
  edge
  [
    source 46
    target 22
  ]
  edge
  [
    source 46
    target 24
  ]
  edge
  [
    source 46
    target 25
  ]
  edge
  [
    source 46
    target 26
  ]
  edge
  [
    source 46
    target 28
  ]
  edge
  [
    source 46
    target 29
  ]
  edge
  [
    source 46
    target 30
  ]
  edge
  [
    source 46
    target 31
  ]
  edge
  [
    source 46
    target 32
  ]
  edge
  [
    source 46
    target 34
  ]
  edge
  [
    source 46
    target 35
  ]
  edge
  [
    source 46
    target 37
  ]
  edge
  [
    source 46
    target 39
  ]
  edge
  [
    source 46
    target 40
  ]
  edge
  [
    source 46
    target 41
  ]
  edge
  [
    source 46
    target 43
  ]
  edge
  [
    source 46
    target 45
  ]
  edge
  [
    source 47
    target 46
  ]
  edge
  [
    source 48
    target 46
  ]
  edge
  [
    source 49
    target 46
  ]
  edge
  [
    source 47
    target 0
  ]
  edge
  [
    source 47
    target 1
  ]
  edge
  [
    source 47
    target 2
  ]
  edge
  [
    source 47
    target 7
  ]
  edge
  [
    source 47
    target 8
  ]
  edge
  [
    source 47
    target 9
  ]
  edge
  [
    source 47
    target 10
  ]
  edge
  [
    source 47
    target 13
  ]
  edge
  [
    source 47
    target 14
  ]
  edge
  [
    source 47
    target 16
  ]
  edge
  [
    source 47
    target 17
  ]
  edge
  [
    source 47
    target 18
  ]
  edge
  [
    source 47
    target 19
  ]
  edge
  [
    source 47
    target 22
  ]
  edge
  [
    source 47
    target 24
  ]
  edge
  [
    source 47
    target 25
  ]
  edge
  [
    source 47
    target 26
  ]
  edge
  [
    source 47
    target 28
  ]
  edge
  [
    source 47
    target 30
  ]
  edge
  [
    source 47
    target 32
  ]
  edge
  [
    source 47
    target 34
  ]
  edge
  [
    source 47
    target 35
  ]
  edge
  [
    source 47
    target 36
  ]
  edge
  [
    source 47
    target 37
  ]
  edge
  [
    source 47
    target 39
  ]
  edge
  [
    source 47
    target 41
  ]
  edge
  [
    source 47
    target 43
  ]
  edge
  [
    source 47
    target 45
  ]
  edge
  [
    source 47
    target 46
  ]
  edge
  [
    source 48
    target 47
  ]
  edge
  [
    source 49
    target 47
  ]
  edge
  [
    source 48
    target 1
  ]
  edge
  [
    source 48
    target 5
  ]
  edge
  [
    source 48
    target 6
  ]
  edge
  [
    source 48
    target 7
  ]
  edge
  [
    source 48
    target 8
  ]
  edge
  [
    source 48
    target 9
  ]
  edge
  [
    source 48
    target 10
  ]
  edge
  [
    source 48
    target 13
  ]
  edge
  [
    source 48
    target 17
  ]
  edge
  [
    source 48
    target 18
  ]
  edge
  [
    source 48
    target 19
  ]
  edge
  [
    source 48
    target 21
  ]
  edge
  [
    source 48
    target 24
  ]
  edge
  [
    source 48
    target 25
  ]
  edge
  [
    source 48
    target 26
  ]
  edge
  [
    source 48
    target 28
  ]
  edge
  [
    source 48
    target 29
  ]
  edge
  [
    source 48
    target 30
  ]
  edge
  [
    source 48
    target 31
  ]
  edge
  [
    source 48
    target 32
  ]
  edge
  [
    source 48
    target 33
  ]
  edge
  [
    source 48
    target 34
  ]
  edge
  [
    source 48
    target 35
  ]
  edge
  [
    source 48
    target 36
  ]
  edge
  [
    source 48
    target 37
  ]
  edge
  [
    source 48
    target 39
  ]
  edge
  [
    source 48
    target 40
  ]
  edge
  [
    source 48
    target 42
  ]
  edge
  [
    source 48
    target 43
  ]
  edge
  [
    source 48
    target 44
  ]
  edge
  [
    source 48
    target 45
  ]
  edge
  [
    source 48
    target 46
  ]
  edge
  [
    source 48
    target 47
  ]
  edge
  [
    source 49
    target 48
  ]
  edge
  [
    source 49
    target 1
  ]
  edge
  [
    source 49
    target 6
  ]
  edge
  [
    source 49
    target 7
  ]
  edge
  [
    source 49
    target 8
  ]
  edge
  [
    source 49
    target 9
  ]
  edge
  [
    source 49
    target 10
  ]
  edge
  [
    source 49
    target 11
  ]
  edge
  [
    source 49
    target 12
  ]
  edge
  [
    source 49
    target 13
  ]
  edge
  [
    source 49
    target 17
  ]
  edge
  [
    source 49
    target 18
  ]
  edge
  [
    source 49
    target 19
  ]
  edge
  [
    source 49
    target 24
  ]
  edge
  [
    source 49
    target 25
  ]
  edge
  [
    source 49
    target 26
  ]
  edge
  [
    source 49
    target 27
  ]
  edge
  [
    source 49
    target 28
  ]
  edge
  [
    source 49
    target 29
  ]
  edge
  [
    source 49
    target 30
  ]
  edge
  [
    source 49
    target 32
  ]
  edge
  [
    source 49
    target 34
  ]
  edge
  [
    source 49
    target 35
  ]
  edge
  [
    source 49
    target 36
  ]
  edge
  [
    source 49
    target 37
  ]
  edge
  [
    source 49
    target 39
  ]
  edge
  [
    source 49
    target 40
  ]
  edge
  [
    source 49
    target 42
  ]
  edge
  [
    source 49
    target 43
  ]
  edge
  [
    source 49
    target 44
  ]
  edge
  [
    source 49
    target 45
  ]
  edge
  [
    source 49
    target 46
  ]
  edge
  [
    source 49
    target 47
  ]
  edge
  [
    source 49
    target 48
  ]
]
