Creator "igraph version 1.2.2 Sun Jan 27 16:07:42 2019"
Version 1
graph
[
  directed 0
  node
  [
    id 0
    name "1"
    community 1
  ]
  node
  [
    id 1
    name "2"
    community 7
  ]
  node
  [
    id 2
    name "3"
    community 4
  ]
  node
  [
    id 3
    name "4"
    community 3
  ]
  node
  [
    id 4
    name "5"
    community 4
  ]
  node
  [
    id 5
    name "6"
    community 7
  ]
  node
  [
    id 6
    name "7"
    community 3
  ]
  node
  [
    id 7
    name "8"
    community 6
  ]
  node
  [
    id 8
    name "9"
    community 5
  ]
  node
  [
    id 9
    name "10"
    community 1
  ]
  node
  [
    id 10
    name "11"
    community 7
  ]
  node
  [
    id 11
    name "12"
    community 10
  ]
  node
  [
    id 12
    name "13"
    community 3
  ]
  node
  [
    id 13
    name "14"
    community 1
  ]
  node
  [
    id 14
    name "15"
    community 9
  ]
  node
  [
    id 15
    name "16"
    community 1
  ]
  node
  [
    id 16
    name "17"
    community 9
  ]
  node
  [
    id 17
    name "18"
    community 10
  ]
  node
  [
    id 18
    name "19"
    community 4
  ]
  node
  [
    id 19
    name "20"
    community 9
  ]
  node
  [
    id 20
    name "21"
    community 4
  ]
  node
  [
    id 21
    name "22"
    community 4
  ]
  node
  [
    id 22
    name "23"
    community 6
  ]
  node
  [
    id 23
    name "24"
    community 2
  ]
  node
  [
    id 24
    name "25"
    community 3
  ]
  node
  [
    id 25
    name "26"
    community 2
  ]
  node
  [
    id 26
    name "27"
    community 1
  ]
  node
  [
    id 27
    name "28"
    community 10
  ]
  node
  [
    id 28
    name "29"
    community 1
  ]
  node
  [
    id 29
    name "30"
    community 2
  ]
  node
  [
    id 30
    name "31"
    community 5
  ]
  node
  [
    id 31
    name "32"
    community 7
  ]
  node
  [
    id 32
    name "33"
    community 5
  ]
  node
  [
    id 33
    name "34"
    community 9
  ]
  node
  [
    id 34
    name "35"
    community 7
  ]
  node
  [
    id 35
    name "36"
    community 9
  ]
  node
  [
    id 36
    name "37"
    community 10
  ]
  node
  [
    id 37
    name "38"
    community 8
  ]
  node
  [
    id 38
    name "39"
    community 6
  ]
  node
  [
    id 39
    name "40"
    community 3
  ]
  node
  [
    id 40
    name "41"
    community 5
  ]
  node
  [
    id 41
    name "42"
    community 2
  ]
  node
  [
    id 42
    name "43"
    community 2
  ]
  node
  [
    id 43
    name "44"
    community 2
  ]
  node
  [
    id 44
    name "45"
    community 6
  ]
  node
  [
    id 45
    name "46"
    community 6
  ]
  node
  [
    id 46
    name "47"
    community 10
  ]
  node
  [
    id 47
    name "48"
    community 6
  ]
  node
  [
    id 48
    name "49"
    community 2
  ]
  node
  [
    id 49
    name "50"
    community 8
  ]
  node
  [
    id 50
    name "51"
    community 4
  ]
  node
  [
    id 51
    name "52"
    community 9
  ]
  node
  [
    id 52
    name "53"
    community 4
  ]
  node
  [
    id 53
    name "54"
    community 8
  ]
  node
  [
    id 54
    name "55"
    community 8
  ]
  node
  [
    id 55
    name "56"
    community 8
  ]
  node
  [
    id 56
    name "57"
    community 4
  ]
  node
  [
    id 57
    name "58"
    community 5
  ]
  node
  [
    id 58
    name "59"
    community 9
  ]
  node
  [
    id 59
    name "60"
    community 2
  ]
  edge
  [
    source 9
    target 0
  ]
  edge
  [
    source 15
    target 0
  ]
  edge
  [
    source 26
    target 0
  ]
  edge
  [
    source 28
    target 0
  ]
  edge
  [
    source 5
    target 1
  ]
  edge
  [
    source 10
    target 1
  ]
  edge
  [
    source 31
    target 1
  ]
  edge
  [
    source 34
    target 1
  ]
  edge
  [
    source 4
    target 2
  ]
  edge
  [
    source 20
    target 2
  ]
  edge
  [
    source 50
    target 2
  ]
  edge
  [
    source 56
    target 2
  ]
  edge
  [
    source 6
    target 3
  ]
  edge
  [
    source 24
    target 3
  ]
  edge
  [
    source 29
    target 3
  ]
  edge
  [
    source 39
    target 3
  ]
  edge
  [
    source 4
    target 2
  ]
  edge
  [
    source 18
    target 4
  ]
  edge
  [
    source 21
    target 4
  ]
  edge
  [
    source 52
    target 4
  ]
  edge
  [
    source 5
    target 1
  ]
  edge
  [
    source 31
    target 5
  ]
  edge
  [
    source 34
    target 5
  ]
  edge
  [
    source 54
    target 5
  ]
  edge
  [
    source 6
    target 3
  ]
  edge
  [
    source 7
    target 6
  ]
  edge
  [
    source 12
    target 6
  ]
  edge
  [
    source 39
    target 6
  ]
  edge
  [
    source 7
    target 6
  ]
  edge
  [
    source 22
    target 7
  ]
  edge
  [
    source 44
    target 7
  ]
  edge
  [
    source 47
    target 7
  ]
  edge
  [
    source 9
    target 8
  ]
  edge
  [
    source 30
    target 8
  ]
  edge
  [
    source 40
    target 8
  ]
  edge
  [
    source 57
    target 8
  ]
  edge
  [
    source 9
    target 0
  ]
  edge
  [
    source 9
    target 8
  ]
  edge
  [
    source 13
    target 9
  ]
  edge
  [
    source 28
    target 9
  ]
  edge
  [
    source 10
    target 1
  ]
  edge
  [
    source 31
    target 10
  ]
  edge
  [
    source 34
    target 10
  ]
  edge
  [
    source 59
    target 10
  ]
  edge
  [
    source 17
    target 11
  ]
  edge
  [
    source 36
    target 11
  ]
  edge
  [
    source 46
    target 11
  ]
  edge
  [
    source 49
    target 11
  ]
  edge
  [
    source 12
    target 6
  ]
  edge
  [
    source 24
    target 12
  ]
  edge
  [
    source 26
    target 12
  ]
  edge
  [
    source 39
    target 12
  ]
  edge
  [
    source 13
    target 9
  ]
  edge
  [
    source 15
    target 13
  ]
  edge
  [
    source 26
    target 13
  ]
  edge
  [
    source 35
    target 13
  ]
  edge
  [
    source 16
    target 14
  ]
  edge
  [
    source 19
    target 14
  ]
  edge
  [
    source 27
    target 14
  ]
  edge
  [
    source 33
    target 14
  ]
  edge
  [
    source 15
    target 0
  ]
  edge
  [
    source 15
    target 13
  ]
  edge
  [
    source 28
    target 15
  ]
  edge
  [
    source 59
    target 15
  ]
  edge
  [
    source 16
    target 14
  ]
  edge
  [
    source 35
    target 16
  ]
  edge
  [
    source 37
    target 16
  ]
  edge
  [
    source 58
    target 16
  ]
  edge
  [
    source 17
    target 11
  ]
  edge
  [
    source 23
    target 17
  ]
  edge
  [
    source 27
    target 17
  ]
  edge
  [
    source 46
    target 17
  ]
  edge
  [
    source 18
    target 4
  ]
  edge
  [
    source 36
    target 18
  ]
  edge
  [
    source 50
    target 18
  ]
  edge
  [
    source 56
    target 18
  ]
  edge
  [
    source 19
    target 14
  ]
  edge
  [
    source 33
    target 19
  ]
  edge
  [
    source 51
    target 19
  ]
  edge
  [
    source 58
    target 19
  ]
  edge
  [
    source 20
    target 2
  ]
  edge
  [
    source 50
    target 20
  ]
  edge
  [
    source 52
    target 20
  ]
  edge
  [
    source 56
    target 20
  ]
  edge
  [
    source 21
    target 4
  ]
  edge
  [
    source 31
    target 21
  ]
  edge
  [
    source 50
    target 21
  ]
  edge
  [
    source 52
    target 21
  ]
  edge
  [
    source 22
    target 7
  ]
  edge
  [
    source 29
    target 22
  ]
  edge
  [
    source 38
    target 22
  ]
  edge
  [
    source 47
    target 22
  ]
  edge
  [
    source 23
    target 17
  ]
  edge
  [
    source 41
    target 23
  ]
  edge
  [
    source 48
    target 23
  ]
  edge
  [
    source 59
    target 23
  ]
  edge
  [
    source 24
    target 3
  ]
  edge
  [
    source 24
    target 12
  ]
  edge
  [
    source 39
    target 24
  ]
  edge
  [
    source 57
    target 24
  ]
  edge
  [
    source 29
    target 25
  ]
  edge
  [
    source 43
    target 25
  ]
  edge
  [
    source 50
    target 25
  ]
  edge
  [
    source 59
    target 25
  ]
  edge
  [
    source 26
    target 0
  ]
  edge
  [
    source 26
    target 12
  ]
  edge
  [
    source 26
    target 13
  ]
  edge
  [
    source 28
    target 26
  ]
  edge
  [
    source 27
    target 14
  ]
  edge
  [
    source 27
    target 17
  ]
  edge
  [
    source 36
    target 27
  ]
  edge
  [
    source 46
    target 27
  ]
  edge
  [
    source 28
    target 0
  ]
  edge
  [
    source 28
    target 9
  ]
  edge
  [
    source 28
    target 15
  ]
  edge
  [
    source 28
    target 26
  ]
  edge
  [
    source 29
    target 3
  ]
  edge
  [
    source 29
    target 22
  ]
  edge
  [
    source 29
    target 25
  ]
  edge
  [
    source 59
    target 29
  ]
  edge
  [
    source 30
    target 8
  ]
  edge
  [
    source 32
    target 30
  ]
  edge
  [
    source 40
    target 30
  ]
  edge
  [
    source 57
    target 30
  ]
  edge
  [
    source 31
    target 1
  ]
  edge
  [
    source 31
    target 5
  ]
  edge
  [
    source 31
    target 10
  ]
  edge
  [
    source 31
    target 21
  ]
  edge
  [
    source 32
    target 30
  ]
  edge
  [
    source 40
    target 32
  ]
  edge
  [
    source 42
    target 32
  ]
  edge
  [
    source 57
    target 32
  ]
  edge
  [
    source 33
    target 14
  ]
  edge
  [
    source 33
    target 19
  ]
  edge
  [
    source 51
    target 33
  ]
  edge
  [
    source 58
    target 33
  ]
  edge
  [
    source 34
    target 1
  ]
  edge
  [
    source 34
    target 5
  ]
  edge
  [
    source 34
    target 10
  ]
  edge
  [
    source 55
    target 34
  ]
  edge
  [
    source 35
    target 13
  ]
  edge
  [
    source 35
    target 16
  ]
  edge
  [
    source 51
    target 35
  ]
  edge
  [
    source 58
    target 35
  ]
  edge
  [
    source 36
    target 11
  ]
  edge
  [
    source 36
    target 18
  ]
  edge
  [
    source 36
    target 27
  ]
  edge
  [
    source 46
    target 36
  ]
  edge
  [
    source 37
    target 16
  ]
  edge
  [
    source 49
    target 37
  ]
  edge
  [
    source 54
    target 37
  ]
  edge
  [
    source 55
    target 37
  ]
  edge
  [
    source 38
    target 22
  ]
  edge
  [
    source 44
    target 38
  ]
  edge
  [
    source 45
    target 38
  ]
  edge
  [
    source 47
    target 38
  ]
  edge
  [
    source 39
    target 3
  ]
  edge
  [
    source 39
    target 6
  ]
  edge
  [
    source 39
    target 12
  ]
  edge
  [
    source 39
    target 24
  ]
  edge
  [
    source 40
    target 8
  ]
  edge
  [
    source 40
    target 30
  ]
  edge
  [
    source 40
    target 32
  ]
  edge
  [
    source 57
    target 40
  ]
  edge
  [
    source 41
    target 23
  ]
  edge
  [
    source 43
    target 41
  ]
  edge
  [
    source 48
    target 41
  ]
  edge
  [
    source 59
    target 41
  ]
  edge
  [
    source 42
    target 32
  ]
  edge
  [
    source 43
    target 42
  ]
  edge
  [
    source 48
    target 42
  ]
  edge
  [
    source 59
    target 42
  ]
  edge
  [
    source 43
    target 25
  ]
  edge
  [
    source 43
    target 41
  ]
  edge
  [
    source 43
    target 42
  ]
  edge
  [
    source 48
    target 43
  ]
  edge
  [
    source 44
    target 7
  ]
  edge
  [
    source 44
    target 38
  ]
  edge
  [
    source 45
    target 44
  ]
  edge
  [
    source 47
    target 44
  ]
  edge
  [
    source 45
    target 38
  ]
  edge
  [
    source 45
    target 44
  ]
  edge
  [
    source 47
    target 45
  ]
  edge
  [
    source 48
    target 45
  ]
  edge
  [
    source 46
    target 11
  ]
  edge
  [
    source 46
    target 17
  ]
  edge
  [
    source 46
    target 27
  ]
  edge
  [
    source 46
    target 36
  ]
  edge
  [
    source 47
    target 7
  ]
  edge
  [
    source 47
    target 22
  ]
  edge
  [
    source 47
    target 38
  ]
  edge
  [
    source 47
    target 44
  ]
  edge
  [
    source 47
    target 45
  ]
  edge
  [
    source 56
    target 47
  ]
  edge
  [
    source 48
    target 23
  ]
  edge
  [
    source 48
    target 41
  ]
  edge
  [
    source 48
    target 42
  ]
  edge
  [
    source 48
    target 43
  ]
  edge
  [
    source 48
    target 45
  ]
  edge
  [
    source 49
    target 11
  ]
  edge
  [
    source 49
    target 37
  ]
  edge
  [
    source 53
    target 49
  ]
  edge
  [
    source 54
    target 49
  ]
  edge
  [
    source 55
    target 49
  ]
  edge
  [
    source 50
    target 2
  ]
  edge
  [
    source 50
    target 18
  ]
  edge
  [
    source 50
    target 20
  ]
  edge
  [
    source 50
    target 21
  ]
  edge
  [
    source 50
    target 25
  ]
  edge
  [
    source 51
    target 19
  ]
  edge
  [
    source 51
    target 33
  ]
  edge
  [
    source 51
    target 35
  ]
  edge
  [
    source 52
    target 51
  ]
  edge
  [
    source 58
    target 51
  ]
  edge
  [
    source 52
    target 4
  ]
  edge
  [
    source 52
    target 20
  ]
  edge
  [
    source 52
    target 21
  ]
  edge
  [
    source 52
    target 51
  ]
  edge
  [
    source 56
    target 52
  ]
  edge
  [
    source 53
    target 49
  ]
  edge
  [
    source 54
    target 53
  ]
  edge
  [
    source 55
    target 53
  ]
  edge
  [
    source 58
    target 53
  ]
  edge
  [
    source 54
    target 5
  ]
  edge
  [
    source 54
    target 37
  ]
  edge
  [
    source 54
    target 49
  ]
  edge
  [
    source 54
    target 53
  ]
  edge
  [
    source 55
    target 54
  ]
  edge
  [
    source 55
    target 34
  ]
  edge
  [
    source 55
    target 37
  ]
  edge
  [
    source 55
    target 49
  ]
  edge
  [
    source 55
    target 53
  ]
  edge
  [
    source 55
    target 54
  ]
  edge
  [
    source 56
    target 2
  ]
  edge
  [
    source 56
    target 18
  ]
  edge
  [
    source 56
    target 20
  ]
  edge
  [
    source 56
    target 47
  ]
  edge
  [
    source 56
    target 52
  ]
  edge
  [
    source 57
    target 8
  ]
  edge
  [
    source 57
    target 24
  ]
  edge
  [
    source 57
    target 30
  ]
  edge
  [
    source 57
    target 32
  ]
  edge
  [
    source 57
    target 40
  ]
  edge
  [
    source 58
    target 16
  ]
  edge
  [
    source 58
    target 19
  ]
  edge
  [
    source 58
    target 33
  ]
  edge
  [
    source 58
    target 35
  ]
  edge
  [
    source 58
    target 51
  ]
  edge
  [
    source 58
    target 53
  ]
  edge
  [
    source 59
    target 10
  ]
  edge
  [
    source 59
    target 15
  ]
  edge
  [
    source 59
    target 23
  ]
  edge
  [
    source 59
    target 25
  ]
  edge
  [
    source 59
    target 29
  ]
  edge
  [
    source 59
    target 41
  ]
  edge
  [
    source 59
    target 42
  ]
]
