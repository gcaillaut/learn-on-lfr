Creator "igraph version 1.2.2 Sun Jan 27 16:07:39 2019"
Version 1
graph
[
  directed 0
  node
  [
    id 0
    name "1"
    community 6
  ]
  node
  [
    id 1
    name "2"
    community 1
  ]
  node
  [
    id 2
    name "3"
    community 6
  ]
  node
  [
    id 3
    name "4"
    community 8
  ]
  node
  [
    id 4
    name "5"
    community 5
  ]
  node
  [
    id 5
    name "6"
    community 1
  ]
  node
  [
    id 6
    name "7"
    community 1
  ]
  node
  [
    id 7
    name "8"
    community 4
  ]
  node
  [
    id 8
    name "9"
    community 7
  ]
  node
  [
    id 9
    name "10"
    community 2
  ]
  node
  [
    id 10
    name "11"
    community 5
  ]
  node
  [
    id 11
    name "12"
    community 7
  ]
  node
  [
    id 12
    name "13"
    community 8
  ]
  node
  [
    id 13
    name "14"
    community 3
  ]
  node
  [
    id 14
    name "15"
    community 2
  ]
  node
  [
    id 15
    name "16"
    community 8
  ]
  node
  [
    id 16
    name "17"
    community 6
  ]
  node
  [
    id 17
    name "18"
    community 7
  ]
  node
  [
    id 18
    name "19"
    community 2
  ]
  node
  [
    id 19
    name "20"
    community 5
  ]
  node
  [
    id 20
    name "21"
    community 6
  ]
  node
  [
    id 21
    name "22"
    community 5
  ]
  node
  [
    id 22
    name "23"
    community 5
  ]
  node
  [
    id 23
    name "24"
    community 2
  ]
  node
  [
    id 24
    name "25"
    community 6
  ]
  node
  [
    id 25
    name "26"
    community 2
  ]
  node
  [
    id 26
    name "27"
    community 3
  ]
  node
  [
    id 27
    name "28"
    community 8
  ]
  node
  [
    id 28
    name "29"
    community 3
  ]
  node
  [
    id 29
    name "30"
    community 4
  ]
  node
  [
    id 30
    name "31"
    community 4
  ]
  node
  [
    id 31
    name "32"
    community 5
  ]
  node
  [
    id 32
    name "33"
    community 8
  ]
  node
  [
    id 33
    name "34"
    community 6
  ]
  node
  [
    id 34
    name "35"
    community 5
  ]
  node
  [
    id 35
    name "36"
    community 5
  ]
  node
  [
    id 36
    name "37"
    community 7
  ]
  node
  [
    id 37
    name "38"
    community 3
  ]
  node
  [
    id 38
    name "39"
    community 7
  ]
  node
  [
    id 39
    name "40"
    community 2
  ]
  node
  [
    id 40
    name "41"
    community 3
  ]
  node
  [
    id 41
    name "42"
    community 3
  ]
  node
  [
    id 42
    name "43"
    community 1
  ]
  node
  [
    id 43
    name "44"
    community 3
  ]
  node
  [
    id 44
    name "45"
    community 4
  ]
  node
  [
    id 45
    name "46"
    community 2
  ]
  node
  [
    id 46
    name "47"
    community 8
  ]
  node
  [
    id 47
    name "48"
    community 8
  ]
  node
  [
    id 48
    name "49"
    community 5
  ]
  node
  [
    id 49
    name "50"
    community 1
  ]
  node
  [
    id 50
    name "51"
    community 7
  ]
  node
  [
    id 51
    name "52"
    community 4
  ]
  node
  [
    id 52
    name "53"
    community 1
  ]
  node
  [
    id 53
    name "54"
    community 1
  ]
  node
  [
    id 54
    name "55"
    community 8
  ]
  node
  [
    id 55
    name "56"
    community 2
  ]
  node
  [
    id 56
    name "57"
    community 2
  ]
  node
  [
    id 57
    name "58"
    community 3
  ]
  node
  [
    id 58
    name "59"
    community 8
  ]
  node
  [
    id 59
    name "60"
    community 3
  ]
  edge
  [
    source 18
    target 0
  ]
  edge
  [
    source 20
    target 0
  ]
  edge
  [
    source 24
    target 0
  ]
  edge
  [
    source 33
    target 0
  ]
  edge
  [
    source 5
    target 1
  ]
  edge
  [
    source 6
    target 1
  ]
  edge
  [
    source 42
    target 1
  ]
  edge
  [
    source 49
    target 1
  ]
  edge
  [
    source 16
    target 2
  ]
  edge
  [
    source 20
    target 2
  ]
  edge
  [
    source 24
    target 2
  ]
  edge
  [
    source 33
    target 2
  ]
  edge
  [
    source 27
    target 3
  ]
  edge
  [
    source 32
    target 3
  ]
  edge
  [
    source 47
    target 3
  ]
  edge
  [
    source 58
    target 3
  ]
  edge
  [
    source 31
    target 4
  ]
  edge
  [
    source 35
    target 4
  ]
  edge
  [
    source 48
    target 4
  ]
  edge
  [
    source 55
    target 4
  ]
  edge
  [
    source 5
    target 1
  ]
  edge
  [
    source 6
    target 5
  ]
  edge
  [
    source 42
    target 5
  ]
  edge
  [
    source 53
    target 5
  ]
  edge
  [
    source 6
    target 1
  ]
  edge
  [
    source 6
    target 5
  ]
  edge
  [
    source 52
    target 6
  ]
  edge
  [
    source 57
    target 6
  ]
  edge
  [
    source 29
    target 7
  ]
  edge
  [
    source 30
    target 7
  ]
  edge
  [
    source 44
    target 7
  ]
  edge
  [
    source 51
    target 7
  ]
  edge
  [
    source 17
    target 8
  ]
  edge
  [
    source 36
    target 8
  ]
  edge
  [
    source 38
    target 8
  ]
  edge
  [
    source 50
    target 8
  ]
  edge
  [
    source 14
    target 9
  ]
  edge
  [
    source 23
    target 9
  ]
  edge
  [
    source 25
    target 9
  ]
  edge
  [
    source 56
    target 9
  ]
  edge
  [
    source 21
    target 10
  ]
  edge
  [
    source 22
    target 10
  ]
  edge
  [
    source 34
    target 10
  ]
  edge
  [
    source 48
    target 10
  ]
  edge
  [
    source 13
    target 11
  ]
  edge
  [
    source 17
    target 11
  ]
  edge
  [
    source 36
    target 11
  ]
  edge
  [
    source 38
    target 11
  ]
  edge
  [
    source 27
    target 12
  ]
  edge
  [
    source 46
    target 12
  ]
  edge
  [
    source 54
    target 12
  ]
  edge
  [
    source 58
    target 12
  ]
  edge
  [
    source 13
    target 11
  ]
  edge
  [
    source 37
    target 13
  ]
  edge
  [
    source 43
    target 13
  ]
  edge
  [
    source 57
    target 13
  ]
  edge
  [
    source 14
    target 9
  ]
  edge
  [
    source 23
    target 14
  ]
  edge
  [
    source 55
    target 14
  ]
  edge
  [
    source 56
    target 14
  ]
  edge
  [
    source 19
    target 15
  ]
  edge
  [
    source 46
    target 15
  ]
  edge
  [
    source 47
    target 15
  ]
  edge
  [
    source 58
    target 15
  ]
  edge
  [
    source 16
    target 2
  ]
  edge
  [
    source 20
    target 16
  ]
  edge
  [
    source 24
    target 16
  ]
  edge
  [
    source 52
    target 16
  ]
  edge
  [
    source 17
    target 8
  ]
  edge
  [
    source 17
    target 11
  ]
  edge
  [
    source 38
    target 17
  ]
  edge
  [
    source 50
    target 17
  ]
  edge
  [
    source 18
    target 0
  ]
  edge
  [
    source 25
    target 18
  ]
  edge
  [
    source 45
    target 18
  ]
  edge
  [
    source 55
    target 18
  ]
  edge
  [
    source 19
    target 15
  ]
  edge
  [
    source 22
    target 19
  ]
  edge
  [
    source 34
    target 19
  ]
  edge
  [
    source 48
    target 19
  ]
  edge
  [
    source 20
    target 0
  ]
  edge
  [
    source 20
    target 2
  ]
  edge
  [
    source 20
    target 16
  ]
  edge
  [
    source 33
    target 20
  ]
  edge
  [
    source 21
    target 10
  ]
  edge
  [
    source 22
    target 21
  ]
  edge
  [
    source 31
    target 21
  ]
  edge
  [
    source 32
    target 21
  ]
  edge
  [
    source 22
    target 10
  ]
  edge
  [
    source 22
    target 19
  ]
  edge
  [
    source 22
    target 21
  ]
  edge
  [
    source 35
    target 22
  ]
  edge
  [
    source 23
    target 9
  ]
  edge
  [
    source 23
    target 14
  ]
  edge
  [
    source 50
    target 23
  ]
  edge
  [
    source 56
    target 23
  ]
  edge
  [
    source 24
    target 0
  ]
  edge
  [
    source 24
    target 2
  ]
  edge
  [
    source 24
    target 16
  ]
  edge
  [
    source 34
    target 24
  ]
  edge
  [
    source 25
    target 9
  ]
  edge
  [
    source 25
    target 18
  ]
  edge
  [
    source 39
    target 25
  ]
  edge
  [
    source 56
    target 25
  ]
  edge
  [
    source 28
    target 26
  ]
  edge
  [
    source 40
    target 26
  ]
  edge
  [
    source 57
    target 26
  ]
  edge
  [
    source 59
    target 26
  ]
  edge
  [
    source 27
    target 3
  ]
  edge
  [
    source 27
    target 12
  ]
  edge
  [
    source 49
    target 27
  ]
  edge
  [
    source 54
    target 27
  ]
  edge
  [
    source 28
    target 26
  ]
  edge
  [
    source 37
    target 28
  ]
  edge
  [
    source 41
    target 28
  ]
  edge
  [
    source 57
    target 28
  ]
  edge
  [
    source 29
    target 7
  ]
  edge
  [
    source 44
    target 29
  ]
  edge
  [
    source 51
    target 29
  ]
  edge
  [
    source 53
    target 29
  ]
  edge
  [
    source 30
    target 7
  ]
  edge
  [
    source 44
    target 30
  ]
  edge
  [
    source 51
    target 30
  ]
  edge
  [
    source 59
    target 30
  ]
  edge
  [
    source 31
    target 4
  ]
  edge
  [
    source 31
    target 21
  ]
  edge
  [
    source 35
    target 31
  ]
  edge
  [
    source 48
    target 31
  ]
  edge
  [
    source 32
    target 3
  ]
  edge
  [
    source 32
    target 21
  ]
  edge
  [
    source 47
    target 32
  ]
  edge
  [
    source 54
    target 32
  ]
  edge
  [
    source 33
    target 0
  ]
  edge
  [
    source 33
    target 2
  ]
  edge
  [
    source 33
    target 20
  ]
  edge
  [
    source 51
    target 33
  ]
  edge
  [
    source 34
    target 10
  ]
  edge
  [
    source 34
    target 19
  ]
  edge
  [
    source 34
    target 24
  ]
  edge
  [
    source 35
    target 34
  ]
  edge
  [
    source 35
    target 4
  ]
  edge
  [
    source 35
    target 22
  ]
  edge
  [
    source 35
    target 31
  ]
  edge
  [
    source 35
    target 34
  ]
  edge
  [
    source 36
    target 8
  ]
  edge
  [
    source 36
    target 11
  ]
  edge
  [
    source 45
    target 36
  ]
  edge
  [
    source 50
    target 36
  ]
  edge
  [
    source 37
    target 13
  ]
  edge
  [
    source 37
    target 28
  ]
  edge
  [
    source 40
    target 37
  ]
  edge
  [
    source 59
    target 37
  ]
  edge
  [
    source 38
    target 8
  ]
  edge
  [
    source 38
    target 11
  ]
  edge
  [
    source 38
    target 17
  ]
  edge
  [
    source 50
    target 38
  ]
  edge
  [
    source 39
    target 25
  ]
  edge
  [
    source 45
    target 39
  ]
  edge
  [
    source 55
    target 39
  ]
  edge
  [
    source 56
    target 39
  ]
  edge
  [
    source 40
    target 26
  ]
  edge
  [
    source 40
    target 37
  ]
  edge
  [
    source 43
    target 40
  ]
  edge
  [
    source 59
    target 40
  ]
  edge
  [
    source 41
    target 28
  ]
  edge
  [
    source 43
    target 41
  ]
  edge
  [
    source 47
    target 41
  ]
  edge
  [
    source 59
    target 41
  ]
  edge
  [
    source 42
    target 1
  ]
  edge
  [
    source 42
    target 5
  ]
  edge
  [
    source 52
    target 42
  ]
  edge
  [
    source 53
    target 42
  ]
  edge
  [
    source 43
    target 13
  ]
  edge
  [
    source 43
    target 40
  ]
  edge
  [
    source 43
    target 41
  ]
  edge
  [
    source 57
    target 43
  ]
  edge
  [
    source 44
    target 7
  ]
  edge
  [
    source 44
    target 29
  ]
  edge
  [
    source 44
    target 30
  ]
  edge
  [
    source 51
    target 44
  ]
  edge
  [
    source 45
    target 18
  ]
  edge
  [
    source 45
    target 36
  ]
  edge
  [
    source 45
    target 39
  ]
  edge
  [
    source 55
    target 45
  ]
  edge
  [
    source 46
    target 12
  ]
  edge
  [
    source 46
    target 15
  ]
  edge
  [
    source 54
    target 46
  ]
  edge
  [
    source 58
    target 46
  ]
  edge
  [
    source 47
    target 3
  ]
  edge
  [
    source 47
    target 15
  ]
  edge
  [
    source 47
    target 32
  ]
  edge
  [
    source 47
    target 41
  ]
  edge
  [
    source 48
    target 4
  ]
  edge
  [
    source 48
    target 10
  ]
  edge
  [
    source 48
    target 19
  ]
  edge
  [
    source 48
    target 31
  ]
  edge
  [
    source 49
    target 1
  ]
  edge
  [
    source 49
    target 27
  ]
  edge
  [
    source 52
    target 49
  ]
  edge
  [
    source 53
    target 49
  ]
  edge
  [
    source 50
    target 8
  ]
  edge
  [
    source 50
    target 17
  ]
  edge
  [
    source 50
    target 23
  ]
  edge
  [
    source 50
    target 36
  ]
  edge
  [
    source 50
    target 38
  ]
  edge
  [
    source 51
    target 7
  ]
  edge
  [
    source 51
    target 29
  ]
  edge
  [
    source 51
    target 30
  ]
  edge
  [
    source 51
    target 33
  ]
  edge
  [
    source 51
    target 44
  ]
  edge
  [
    source 52
    target 6
  ]
  edge
  [
    source 52
    target 16
  ]
  edge
  [
    source 52
    target 42
  ]
  edge
  [
    source 52
    target 49
  ]
  edge
  [
    source 53
    target 52
  ]
  edge
  [
    source 53
    target 5
  ]
  edge
  [
    source 53
    target 29
  ]
  edge
  [
    source 53
    target 42
  ]
  edge
  [
    source 53
    target 49
  ]
  edge
  [
    source 53
    target 52
  ]
  edge
  [
    source 54
    target 12
  ]
  edge
  [
    source 54
    target 27
  ]
  edge
  [
    source 54
    target 32
  ]
  edge
  [
    source 54
    target 46
  ]
  edge
  [
    source 58
    target 54
  ]
  edge
  [
    source 55
    target 4
  ]
  edge
  [
    source 55
    target 14
  ]
  edge
  [
    source 55
    target 18
  ]
  edge
  [
    source 55
    target 39
  ]
  edge
  [
    source 55
    target 45
  ]
  edge
  [
    source 56
    target 9
  ]
  edge
  [
    source 56
    target 14
  ]
  edge
  [
    source 56
    target 23
  ]
  edge
  [
    source 56
    target 25
  ]
  edge
  [
    source 56
    target 39
  ]
  edge
  [
    source 57
    target 6
  ]
  edge
  [
    source 57
    target 13
  ]
  edge
  [
    source 57
    target 26
  ]
  edge
  [
    source 57
    target 28
  ]
  edge
  [
    source 57
    target 43
  ]
  edge
  [
    source 58
    target 3
  ]
  edge
  [
    source 58
    target 12
  ]
  edge
  [
    source 58
    target 15
  ]
  edge
  [
    source 58
    target 46
  ]
  edge
  [
    source 58
    target 54
  ]
  edge
  [
    source 59
    target 26
  ]
  edge
  [
    source 59
    target 30
  ]
  edge
  [
    source 59
    target 37
  ]
  edge
  [
    source 59
    target 40
  ]
  edge
  [
    source 59
    target 41
  ]
]
