Creator "igraph version 1.2.2 Sun Jan 27 16:08:40 2019"
Version 1
graph
[
  directed 0
  node
  [
    id 0
    name "1"
    community 4
  ]
  node
  [
    id 1
    name "2"
    community 4
  ]
  node
  [
    id 2
    name "3"
    community 3
  ]
  node
  [
    id 3
    name "4"
    community 1
  ]
  node
  [
    id 4
    name "5"
    community 5
  ]
  node
  [
    id 5
    name "6"
    community 3
  ]
  node
  [
    id 6
    name "7"
    community 2
  ]
  node
  [
    id 7
    name "8"
    community 2
  ]
  node
  [
    id 8
    name "9"
    community 2
  ]
  node
  [
    id 9
    name "10"
    community 1
  ]
  node
  [
    id 10
    name "11"
    community 5
  ]
  node
  [
    id 11
    name "12"
    community 4
  ]
  node
  [
    id 12
    name "13"
    community 4
  ]
  node
  [
    id 13
    name "14"
    community 2
  ]
  node
  [
    id 14
    name "15"
    community 4
  ]
  node
  [
    id 15
    name "16"
    community 5
  ]
  node
  [
    id 16
    name "17"
    community 2
  ]
  node
  [
    id 17
    name "18"
    community 5
  ]
  node
  [
    id 18
    name "19"
    community 3
  ]
  node
  [
    id 19
    name "20"
    community 3
  ]
  node
  [
    id 20
    name "21"
    community 1
  ]
  node
  [
    id 21
    name "22"
    community 3
  ]
  node
  [
    id 22
    name "23"
    community 1
  ]
  node
  [
    id 23
    name "24"
    community 5
  ]
  node
  [
    id 24
    name "25"
    community 5
  ]
  node
  [
    id 25
    name "26"
    community 3
  ]
  node
  [
    id 26
    name "27"
    community 1
  ]
  node
  [
    id 27
    name "28"
    community 1
  ]
  node
  [
    id 28
    name "29"
    community 1
  ]
  node
  [
    id 29
    name "30"
    community 1
  ]
  node
  [
    id 30
    name "31"
    community 1
  ]
  node
  [
    id 31
    name "32"
    community 1
  ]
  node
  [
    id 32
    name "33"
    community 1
  ]
  node
  [
    id 33
    name "34"
    community 1
  ]
  edge
  [
    source 14
    target 0
  ]
  edge
  [
    source 33
    target 0
  ]
  edge
  [
    source 14
    target 1
  ]
  edge
  [
    source 29
    target 1
  ]
  edge
  [
    source 19
    target 2
  ]
  edge
  [
    source 25
    target 2
  ]
  edge
  [
    source 32
    target 3
  ]
  edge
  [
    source 33
    target 3
  ]
  edge
  [
    source 23
    target 4
  ]
  edge
  [
    source 24
    target 4
  ]
  edge
  [
    source 21
    target 5
  ]
  edge
  [
    source 25
    target 5
  ]
  edge
  [
    source 7
    target 6
  ]
  edge
  [
    source 8
    target 6
  ]
  edge
  [
    source 7
    target 6
  ]
  edge
  [
    source 16
    target 7
  ]
  edge
  [
    source 8
    target 6
  ]
  edge
  [
    source 16
    target 8
  ]
  edge
  [
    source 25
    target 9
  ]
  edge
  [
    source 33
    target 9
  ]
  edge
  [
    source 23
    target 10
  ]
  edge
  [
    source 24
    target 10
  ]
  edge
  [
    source 12
    target 11
  ]
  edge
  [
    source 14
    target 11
  ]
  edge
  [
    source 12
    target 11
  ]
  edge
  [
    source 22
    target 12
  ]
  edge
  [
    source 16
    target 13
  ]
  edge
  [
    source 24
    target 13
  ]
  edge
  [
    source 14
    target 0
  ]
  edge
  [
    source 14
    target 1
  ]
  edge
  [
    source 14
    target 11
  ]
  edge
  [
    source 17
    target 15
  ]
  edge
  [
    source 23
    target 15
  ]
  edge
  [
    source 24
    target 15
  ]
  edge
  [
    source 16
    target 7
  ]
  edge
  [
    source 16
    target 8
  ]
  edge
  [
    source 16
    target 13
  ]
  edge
  [
    source 17
    target 15
  ]
  edge
  [
    source 23
    target 17
  ]
  edge
  [
    source 24
    target 17
  ]
  edge
  [
    source 19
    target 18
  ]
  edge
  [
    source 21
    target 18
  ]
  edge
  [
    source 25
    target 18
  ]
  edge
  [
    source 19
    target 2
  ]
  edge
  [
    source 19
    target 18
  ]
  edge
  [
    source 21
    target 19
  ]
  edge
  [
    source 25
    target 19
  ]
  edge
  [
    source 29
    target 20
  ]
  edge
  [
    source 31
    target 20
  ]
  edge
  [
    source 32
    target 20
  ]
  edge
  [
    source 33
    target 20
  ]
  edge
  [
    source 21
    target 5
  ]
  edge
  [
    source 21
    target 18
  ]
  edge
  [
    source 21
    target 19
  ]
  edge
  [
    source 25
    target 21
  ]
  edge
  [
    source 32
    target 21
  ]
  edge
  [
    source 22
    target 12
  ]
  edge
  [
    source 27
    target 22
  ]
  edge
  [
    source 31
    target 22
  ]
  edge
  [
    source 32
    target 22
  ]
  edge
  [
    source 33
    target 22
  ]
  edge
  [
    source 23
    target 4
  ]
  edge
  [
    source 23
    target 10
  ]
  edge
  [
    source 23
    target 15
  ]
  edge
  [
    source 23
    target 17
  ]
  edge
  [
    source 24
    target 23
  ]
  edge
  [
    source 24
    target 4
  ]
  edge
  [
    source 24
    target 10
  ]
  edge
  [
    source 24
    target 13
  ]
  edge
  [
    source 24
    target 15
  ]
  edge
  [
    source 24
    target 17
  ]
  edge
  [
    source 24
    target 23
  ]
  edge
  [
    source 25
    target 2
  ]
  edge
  [
    source 25
    target 5
  ]
  edge
  [
    source 25
    target 9
  ]
  edge
  [
    source 25
    target 18
  ]
  edge
  [
    source 25
    target 19
  ]
  edge
  [
    source 25
    target 21
  ]
  edge
  [
    source 27
    target 26
  ]
  edge
  [
    source 28
    target 26
  ]
  edge
  [
    source 30
    target 26
  ]
  edge
  [
    source 31
    target 26
  ]
  edge
  [
    source 32
    target 26
  ]
  edge
  [
    source 33
    target 26
  ]
  edge
  [
    source 27
    target 22
  ]
  edge
  [
    source 27
    target 26
  ]
  edge
  [
    source 28
    target 27
  ]
  edge
  [
    source 30
    target 27
  ]
  edge
  [
    source 31
    target 27
  ]
  edge
  [
    source 32
    target 27
  ]
  edge
  [
    source 33
    target 27
  ]
  edge
  [
    source 28
    target 26
  ]
  edge
  [
    source 28
    target 27
  ]
  edge
  [
    source 29
    target 28
  ]
  edge
  [
    source 30
    target 28
  ]
  edge
  [
    source 31
    target 28
  ]
  edge
  [
    source 32
    target 28
  ]
  edge
  [
    source 33
    target 28
  ]
  edge
  [
    source 29
    target 1
  ]
  edge
  [
    source 29
    target 20
  ]
  edge
  [
    source 29
    target 28
  ]
  edge
  [
    source 30
    target 29
  ]
  edge
  [
    source 31
    target 29
  ]
  edge
  [
    source 32
    target 29
  ]
  edge
  [
    source 33
    target 29
  ]
  edge
  [
    source 30
    target 26
  ]
  edge
  [
    source 30
    target 27
  ]
  edge
  [
    source 30
    target 28
  ]
  edge
  [
    source 30
    target 29
  ]
  edge
  [
    source 31
    target 30
  ]
  edge
  [
    source 32
    target 30
  ]
  edge
  [
    source 33
    target 30
  ]
  edge
  [
    source 31
    target 20
  ]
  edge
  [
    source 31
    target 22
  ]
  edge
  [
    source 31
    target 26
  ]
  edge
  [
    source 31
    target 27
  ]
  edge
  [
    source 31
    target 28
  ]
  edge
  [
    source 31
    target 29
  ]
  edge
  [
    source 31
    target 30
  ]
  edge
  [
    source 32
    target 31
  ]
  edge
  [
    source 33
    target 31
  ]
  edge
  [
    source 32
    target 3
  ]
  edge
  [
    source 32
    target 20
  ]
  edge
  [
    source 32
    target 21
  ]
  edge
  [
    source 32
    target 22
  ]
  edge
  [
    source 32
    target 26
  ]
  edge
  [
    source 32
    target 27
  ]
  edge
  [
    source 32
    target 28
  ]
  edge
  [
    source 32
    target 29
  ]
  edge
  [
    source 32
    target 30
  ]
  edge
  [
    source 32
    target 31
  ]
  edge
  [
    source 33
    target 32
  ]
  edge
  [
    source 33
    target 0
  ]
  edge
  [
    source 33
    target 3
  ]
  edge
  [
    source 33
    target 9
  ]
  edge
  [
    source 33
    target 20
  ]
  edge
  [
    source 33
    target 22
  ]
  edge
  [
    source 33
    target 26
  ]
  edge
  [
    source 33
    target 27
  ]
  edge
  [
    source 33
    target 28
  ]
  edge
  [
    source 33
    target 29
  ]
  edge
  [
    source 33
    target 30
  ]
  edge
  [
    source 33
    target 31
  ]
  edge
  [
    source 33
    target 32
  ]
]
