library("parallel")
library("igraph")
library("magrittr")

import_data <- function(n, base_dir = "graphs", g = NULL) {
  if (is.null(g)) {
    filename <- if (identical(tools::file_ext(n), "gml")) n else paste0(n, ".gml")
    g <- simplify(read_graph(file.path(base_dir, filename), format = "gml"))
  }

  community <- V(g)$community

  universe <- V(g)$name
  if (is.null(universe)) {
    universe <- as.character(seq_len(vcount(g)))
  }

  adj <- as_adjacency_matrix(g, sparse = FALSE)
  adj[adj > 0] <- 1 # When g is directed, as_adjacency_matrix writes 2 when an edge is bidirectionnal
  diag(adj) <- 1

  target <-
    vapply(community, function(c1) {
      vapply(community, function(c2) any(c1 %in% c2), logical(1L))
    }, logical(vcount(g))) %>%
    t(.)

  opinions <- carryover_opinion_matrix(adj)

  list(name = n, universe = universe, adj = adj, community = community, target = target, opinions = opinions)
}

make_typev_predicates <- function(adj, opinions) {
  list(
    adj = neighborhoods_predicate(adj),
    nr = neighbors_ratio_predicate(adj) %>% threshold_predicate(., 0.3),

    co30 = opinions %>% fuzzy_neighborhoods_predicate(.) %>% threshold_predicate(., 0.3),
    co15 = opinions %>% fuzzy_neighborhoods_predicate(.) %>% threshold_predicate(., 0.15),

    i2 = intersection_size_predicate(adj) %>% threshold_predicate(., 2L),
    i3 = intersection_size_predicate(adj) %>% threshold_predicate(., 3L),
    i4 = intersection_size_predicate(adj) %>% threshold_predicate(., 4L),

    luoish = luoish_predicate(adj)
  )
}

make_all_predicates <- function(adj, opinions) {
  typev <- make_typev_predicates(adj, opinions)

  others <-
    list(
      ivu = intersection_vs_union_predicate(adj) %>% threshold_predicate(., 0.3),
      sr = set_ratio(adj) %>% threshold_predicate(., 0.3),
      nm = neighborhood_matching_predicate(adj) %>% threshold_predicate(., 0.3),

      clauset = clauset_predicate(adj),
      luo = luo_predicate(adj),
      chen = chen_predicate(adj)
    )
  
  c(typev, others)
}

make_fuzzy_predicates <- function(adj, opinions) {
  list(
    adj = fuzzy_neighborhoods_predicate(adj),
    nr = fuzzy_neighbors_ratio_predicate(adj),

    co = opinions %>% fuzzy_neighborhoods_predicate(.),

    ivu = fuzzy_intersection_vs_union_predicate(adj),
    sr = fuzzy_set_ratio_predicate(adj),
    nm = fuzzy_neighborhood_matching_predicate(adj),

    fuzzyluo1 = fuzzy_luo_predicate1(adj),
    fuzzyluo2 = fuzzy_luo_predicate2(adj)
  )
}

make_training_subset <- function(community, n, universe) {
  communities <-
    if (is.list(community)) unique(do.call(c, community))
    else unique(community)

  res <- lapply(communities, function(x) {
    node_in_community <- which(vapply(community, function(com) x %in% com, logical(1L)))
    sample(node_in_community, min(length(node_in_community), n))
  })
  universe[as.integer(do.call(c, res))]
}


parse_dnf <- function(dnf_str) {
  if (identical(dnf_str, "()")) {
    return(list())
  }

  clauses <- str_split(dnf_str, pattern = fixed(" + "))
  lits <-
    clauses[[1]] %>%
    str_sub(., start = 2L, end = -2L) %>%
    str_split(., pattern = fixed("."))

  lits
}

make_cluster <- function(n_cores) {
    if (identical(.Platform$OS.type, "windows")) {
      cl <- makeCluster(n_cores, outfile = "")
      clusterEvalQ(cl, {
        library("tidyverse")
        library("magrittr")
        library("igraph")
        source("expe.R")
      })
      cl
    }
    else {
      makeCluster(n_cores, type = "FORK", outfile = "")
    }
}